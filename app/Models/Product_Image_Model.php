<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Image_Model extends Model
{
    protected $table = 'product_images';

    protected $fillable = [
        'name',
        'alternate_text',
        'image_size',
        'case_id'
    ];

    public function cases() {
        return $this->belongsTo(Case_Model::class);
    }
}
