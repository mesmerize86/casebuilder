<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Case_Model extends Model
{
    protected $table = 'cases';

    /**
     * name            => name of case
     * caseCode        => unique name for case
     * caseType        => type of case
     * numberOfBottles => number of unique bottles in a case
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'caseCode',
        'caseType',
        'numberOfBottles'
    ];

    public function bottles() {
        return $this->belongsToMany(Bottle_Model::class, 'bottle_case', 'case_id', 'bottle_id');
    }

    public function campaigns() {
        return $this->belongsToMany(Campaign_Model::class, 'campaign_case', 'case_id', 'campaign_id');
    }

    public function product_images() {
        return $this->hasOne(Product_Image_Model::class, 'case_id', 'id');
    }
}
