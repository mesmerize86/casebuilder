<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Bottle_Model extends Model
{
    protected $table = 'bottles';

    /**
     * name     => name of bottle
     * skuCode  => sku code of bottle
     * itemCode => unique code of bottle
     *
     * @var string[]
     *
     */
    protected $fillable = [
        'name',
        'skuCode',
        'itemCode'
    ];

    public function cases() {
        return $this->belongsToMany(Case_Model::class, 'bottle_case', 'bottle_id', 'case_id');
    }
}
