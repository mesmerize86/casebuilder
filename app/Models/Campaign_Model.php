<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Stmt\Case_;

class Campaign_Model extends Model
{
    protected $table = 'campaigns';

    protected $fillable = [
      'name',
      'campaignCode'
    ];

    public function cases() {
        return $this->belongsToMany(Case_Model::class, 'campaign_case', 'campaign_id', 'case_id');
    }
}
