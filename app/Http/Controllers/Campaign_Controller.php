<?php

namespace App\Http\Controllers;

use App\Models\Campaign_Model;
use App\Models\Case_Model;
use Facades\App\Repositories\CacheHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class Campaign_Controller extends Controller
{
    CONST CACHE_KEY = 'CAMPAIGN';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the campaigns and campaign content
        $campaigns = Campaign_Model::with(['cases' => function($query) {
            $query->with('product_images');
        }])->get();
        foreach($campaigns as $campaign) {
            $campaign['campaignContent'] = $campaign['cases'];
            unset($campaign['cases']);
        }
        return response()->json($campaigns, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'hello';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate_input_data = $request->validate([
            'name' => 'required',
            'campaignCode' => 'required',
            'campaignContent' => 'required'
        ]);

        if($validate_input_data) {
            $campaign = new Campaign_Model();
            $campaign->create($validate_input_data);

            $campaign_id = Campaign_Model::latest()->first();
            $campaign_content = $request->input('campaignContent');
            forEach($campaign_content as $content){
                $case_model = Case_Model::find($content['value']);
                //link relationship
                $case_model->campaigns()->attach($campaign_id);
            }
            return 'success';
        } else {
            return 'fail';
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($campaignCode)
    {
        $error = [];
        $success = [];
        $response = [];
        $responseStatus = '';

        $key = "campaign_code.{$campaignCode}";
        $cacheKey = CacheHelper::getCacheKey(self::CACHE_KEY, $key);
        //retrieving or storing cache
        $campaign = Cache::remember($cacheKey, Carbon::now()->addMinutes(2), function()use($campaignCode){
            return Campaign_Model::with(['cases' => function($query) {
                $query->with('product_images');
            }])->where('campaignCode', $campaignCode)->first();
        });

        //if record not found,
        if(!$campaign) {
            $error['message'] = 'Campaign code doesn\'t match';
            $error['status'] = 0;
            $error['statusCode'] = 404;
            $response['error'] = $error;
            $responseStatus = 404;
        } else {
            /* renaming cases to campaignContents*/
            $campaign['campaignContents'] = $campaign['cases'];
            unset($campaign['cases']); //removing cases so we do not have repeated

            $success['message'] = 'Successfully Found.';
            $success['status'] = 1;
            $success['statusCode'] = 200;
            $response['success'] = $success;
            $response['campaign'] = $campaign;
            $responseStatus = 200;
        }

        return response()->json($response, $responseStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deployToPhotoshop(Request $request) {
        $data = json_encode($request->all());
        $filename = 'data.json';
        $handle = fopen($filename, 'w+');
        fputs($handle, $data);
        fclose($handle);
        $headers = array('Content-type' => 'application/json');
        return response()->download(public_path($filename));
    }
}
