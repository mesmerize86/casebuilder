<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bottle_Model;
use App\Models\Case_Model;

class Case_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the cases and case content
        $cases = Case_Model::with('product_images')->with('bottles')->get();
        foreach($cases as $key => $case) {
            $case['caseContents'] = $case['bottles'];
            unset($case['bottles']);
        }


        return response()->json($cases, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated_input_data = $request->validate([
            'name' => 'required',
            'caseCode' => 'required|unique:App\Models\Case_model,caseCode',
            'caseType' => 'required',
            'numberOfBottles' => 'required',
            'caseContent' => 'required'
        ]);

        if ($validated_input_data) {
            $case = new Case_Model();

            $case->create([
                    'name' => $request->input('name'),
                    'caseCode' => $request->input('caseCode'),
                    'caseType' => $request->input('caseType'),
                    'numberOfBottles' => $request->input('numberOfBottles'),
                ]);

            $case_id = Case_Model::latest()->first();
            $case_content = $request->input('caseContent');
            foreach($case_content as $key => $bottle) {
                //based on id, get the bottle model
                $bottle_model = Bottle_Model::find($bottle['value']);
                //link relationship
                $bottle_model->cases()->attach($case_id);
            }
            $success = [
              'status' => 200,
              'message' => 'You have Successfully Saved.'
            ];
            return response()->json($success, 200);
        } else {
            return $validated_input_data;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($caseCode)
    {
        $error = [];
        $success = [];
        $response = [];
        $responseStatus = '';

        //if record not found,
        $case = Case_Model::with('product_images')->with('bottles')->where('caseCode', $caseCode)->first();
        if(!$case ) {
            $error['message'] = 'CaseCode doesn\'t match';
            $error['status'] = 0;
            $error['statusCode'] = 404;
            $response['error'] = $error;
            $responseStatus = 404;
        } else {
            /*$case['caseContents'] = $case['bottles'];
            unset($case['bottles']);

            $success['message'] = 'Successfully Found.';
            $success['status'] = 1;
            $success['statusCode'] = 200;
            $response['success'] = $success;
            $response['case'] = $case;
            $responseStatus = 200;*/
        }

        return response()->json($case);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function subArraysToString($ar, $sep = ', ') {
        $str = '';
        foreach ($ar as $val) {
            $str .= implode($sep, $val);
            $str .= $sep; // add separator between sub-arrays
        }
        $str = rtrim($str, $sep); // remove last separator
        return $str;
    }
}
