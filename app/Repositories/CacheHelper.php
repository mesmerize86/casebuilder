<?php

namespace App\Repositories;
class CacheHelper
{
    public function getCacheKey($cache_key, $key)
    {
        $key = strtoupper($key);
        return $cache_key.".$key";
    }
}
