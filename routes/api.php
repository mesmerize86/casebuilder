<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Http\Controllers\AccessTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('bottles', 'Bottle_Controller');
Route::apiResource('cases', 'Case_Controller');
Route::apiResource('cases/{$caseCode}', 'Case_Controller@show');
Route::post('cases', 'Case_Controller@store');
Route::apiResource('campaigns', 'Campaign_Controller');
Route::post('campaign', 'Campaign_Controller@store');
Route::apiResource('campaigns/{$campaignCode}', 'Campaign_Controller@show');
Route::apiResource('campaigns/{$campaignCode}', 'Campaign_Controller@show');
Route::get('deployToPhotoshop', 'Campaign_Controller@deployToPhotoshop');
Route::post('login', [AccessTokenController::class, 'issueToken'])->middleware(['api-login', 'throttle']);


