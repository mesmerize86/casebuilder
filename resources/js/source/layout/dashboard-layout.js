import React from 'react';
import Header from '../shared-components/header';

const DashboardLayout = (props) => {
    return (
        <div className="dashboard-layout">
            <div className="topbar">
                <div className="topbar-container">
                </div>
            </div>
            <Header />
            { props.children }
        </div>
    );
};

export default DashboardLayout;
