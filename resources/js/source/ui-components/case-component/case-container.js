import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

/* utility function */
import { handleImageError } from '../../helpers/utility';

const CaseContainer = (props) => {
    const {name, caseCode, product_images } = props.cases;

    const [isDisabled, setIsDisabled] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [addedToBasket, setAddedToBasket] = useState(false);


    useEffect(() => {

        // Update the document title using the browser API
        if(isLoading && !props.case.isLoading) {
            setTimeout(function(){
                setIsLoading(props.case.isLoading);
            }, 800);
        }

    });


    //click events:
    //handle add to basket event
    const handleAddToBasket = (cases, index)=> {
        props.handleAddToBasket(cases, index);
        setIsLoading(true);
        // setAddedItemToBasket(addedItemToBasket => [cases, ...addedItemToBasket]);
    }


    /* initialize case image object */
    let images = {
        name: '',
        alt_text: ''
    };

    /* if there are no case images then add image not available*/
    if (product_images) {
        images = {
            name : product_images.name,
            alt_text: product_images.alternate_text
        }
    }
    return (
        <div className="case-component">
            <p className="case-title">{ name } | { caseCode }</p>
            <div className="case-block-image">
                <img src={ handleImageError( images.name) } alt={images.alt_text} className="img-responsive"/>
            </div>
            <input type="button" className={classnames('button button-secondary', {'is-loading': isLoading})} value={(isLoading) ? "adding..." : "Add to basket"} onClick={(e) => handleAddToBasket(props.cases, props.index)} disabled={isDisabled}/>
        </div>
    );
};

const mapStateToProps = (state)=> ({
    caseItem: state.cases,
    case: state.cases,
    baskets: state.basket
});

export default connect(mapStateToProps, null)(CaseContainer);
