import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';

/* Actions */
import { getCaseByCaseCode } from "../../services/cases/caseAction";

/* components */
import CaseContainer from './case-container';

const CaseComponent = (props) => {

    const [addedItemToBasket, setAddedItemToBasket] = useState([]);

    //click events:
    //handle add to basket
    const handleAddToBasket = (cases, index)=> {
        props.getCaseByCaseCode(cases.caseCode, index);
        setAddedItemToBasket(addedItemToBasket => [...addedItemToBasket, cases]);

    }

    console.log('case component', typeof props.campaignContents);
    // looping case content
    const caseContainer = props.campaignContents.map((cases, index) => {
        return ( <CaseContainer key={index} cases={cases} index={index} handleAddToBasket={handleAddToBasket}/> )
    });
    return (
        <div className="container">
            <div className="flex-row flex-wrap">
                { caseContainer }
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    basket: state.basket
});

export default connect(mapStateToProps, { getCaseByCaseCode })(CaseComponent);
