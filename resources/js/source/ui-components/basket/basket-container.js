import React from 'react';
import { Button } from '../../ui/molecules/form-component';

import { handleImageError } from '../../helpers/utility';

const BasketContainer = (props) => {
    const itemsInBasket = props.basketItem.items;
    const items = itemsInBasket.map((item, index) => {
        return (
            <div key={item.caseCode} className="basket-items-list row">
                <img className="basket-item col-xs-4 img-fluid" src={handleImageError(item.product_images.name)} alt={item.product_images.alternate_text }/>
                <span className="basket-item col-xs-4">{item.name}</span>
                <span className="basket-item col-xs-2">{item.caseCode}</span>
                <span className="basket-item icon icon-close col-xs-2" onClick={() => props.removeItemFromBasket(index)}></span>
            </div>
        )
    })
    return (
        <div className="basket-items">
            <form onSubmit={(e) => props.deployToPhotoshop(e, itemsInBasket)} >
                { props.basketItem && items }
                <div className="divider"></div>
                <Button type="submit" value="Deploy To Photoshop" buttonType="secondary" extraClass="full_width"/>
            </form>
        </div>
    );
};

export default BasketContainer;
