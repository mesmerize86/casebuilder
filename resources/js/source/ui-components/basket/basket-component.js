import React, {useState, useEffect}  from 'react';
import { connect } from 'react-redux';
import BasketContainer from "./basket-container";
import { updateBasket } from '../../services/basket/basketAction';
import { deployToPhotoshop } from '../../services/deployToPhotoshop/deployToPhotoshopAction';
import ModalPopup from '../../ui/molecules/modal-popup-molecule';

const BasketComponent = (props)=> {
    //state
    const [count, setCount] = useState(0);
    const [b_open_basket_dropdown, set_b_open_basket_dropdown] = useState(false);

    //click events:
    //remove item from basket
    const removeItemFromBasket = (index)=> {
        if (props.basket.count < 1) {
            return false;
        }
        props.updateBasket(index);
    }

    //click events:
    //toggle basket items on hover
    const handleOpenModal = ()=> {
        set_b_open_basket_dropdown(true);
    }

    const handleCloseModal = ()=> {
      set_b_open_basket_dropdown(false);
    }

    //click events:
    // deploy to photoshop
    const deployToPhotoshop = (e, basketItems)=> {
        e.preventDefault();
        console.log('deploy to photoshop');
        //this.props.deployToPhotoshop(basketItems);
    }

    return (
        <div>
            <div className="header-basket" onMouseEnter={ handleOpenModal } >
                <span>{ props.basket.count }</span>
                {(props.basket.count > 0) && <span className="icon icon-ctrl basket_icon_ctrl"></span> }
            </div>
            {
              <ModalPopup isModalOpen={ b_open_basket_dropdown} handleCloseModal={ handleCloseModal }>
                <BasketContainer
                  basketItem={props.basket}
                  removeItemFromBasket={removeItemFromBasket}
                  handleCloseModal={ handleCloseModal }
                  deployToPhotoshop={deployToPhotoshop}/>
              </ModalPopup>
            }
        </div>
    )
}


const mapStateToProps = (state) => ({
    basket: state.basket
});

export default connect(mapStateToProps, { updateBasket, deployToPhotoshop })(BasketComponent);

