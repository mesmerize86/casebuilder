import React, { useState }  from 'react';
import {useForm} from 'react-hook-form';
import axios from 'axios';
import Select from 'react-select';


const Home = () => {
    const {register,handleSubmit} = useForm();
    const [selectedOption, setSelectedOption] = useState(null);
    const onSubmit = (data)=> {
        const newData = data;
        newData['campaignContent'] = selectedOption;
        axios.post('/api/campaigns', newData).then(response =>{
            console.log(response);
            debugger;
        }).catch(err => {
            console.log(err);
            debugger;
        })
    }

    const options = [
        { value: '1', label: 'mixed red' },
        { value: '2', label: 'Mixed White' },
        { value: '39', label: 'merlot case' },
        { value: '49', label: 'test5' },
    ];
    const handleChange = (e)=> {
        setSelectedOption(e);
    }


    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <span>Name:</span><input type="text" name="name" ref={register}/>
                        </div>
                        <div><span>Campaign code:</span><input type="text" name="campaignCode" ref={register}/></div>
                        <div>
                            <Select value={selectedOption} onChange={handleChange} options={options} isMulti />
                        </div>
                        <div><input type="submit" value="Submit"/></div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Home;
