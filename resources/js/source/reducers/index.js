import { combineReducers } from 'redux';
import campaigns from '../services/campaigns/campaignReducer';
import cases from '../services/cases/caseReducer';
import basket from '../services/basket/basketReducer';
import deployToPhotoshop from '../services/deployToPhotoshop/deployToPhotoshopReducer';
import authService from '../services/auth/authServiceReducer';

export default combineReducers({
    campaigns,
    cases,
    basket,
    deployToPhotoshop,
    authService
});
