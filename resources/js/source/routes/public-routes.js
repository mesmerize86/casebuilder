import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from "../helpers/auth";

const PublicRoutes = ({ component: Component, layout: Layout, ...rest }) => {
    return (
        <Route {...rest} render={ props => {
                return <Layout><Component {...props} /></Layout>
        }}/>
    );
}

export default PublicRoutes;
