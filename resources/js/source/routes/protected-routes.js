import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from "../helpers/auth";

const ProtectedRoutes = ({ component: Component, layout: Layout, ...rest }) => {
    return (
        <Route {...rest} render={ props => {
            if (isAuthenticated()) {
                return <Layout><Component {...props} /></Layout>
            } else {
                return <Redirect to='/' />
            }
        }}/>
    );
}

export default ProtectedRoutes;
