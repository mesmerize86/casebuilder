import React from 'react';
import { Switch, Route } from 'react-router-dom';

/* Layout Route */
import LayoutRoutes from "./layout-routes";
import ProtectedRoutes from "./protected-routes";
import PublicRoutes from "./public-routes";

/* Layout */
import DashboardLayout from "../layout/dashboard-layout";
import EmptyLayout from "../layout/empty-layout";

/* Page Component */
import HomeComponent from '../pages/home';
import BuildSingleBottles from '../pages/build-single-bottles';
import BuildCases from '../pages/build-cases';
import BuildCampaign from '../pages/build-campaigns/build-campaign';
import LoginPage from '../pages/backend/login/login';

const routes = () => {
    return (
        <Switch>
            <PublicRoutes exact path="/" component={ LoginPage } layout={ EmptyLayout } />
            <PublicRoutes path="/login" component={ LoginPage } layout={ EmptyLayout } />
            <ProtectedRoutes path="/build_campaigns" component={ BuildCampaign } layout={ DashboardLayout } />
            <ProtectedRoutes path="/build_cases" component={ BuildCases } layout={ DashboardLayout } />
            <ProtectedRoutes path="/build_single_bottles" component={ BuildSingleBottles } layout={ DashboardLayout } />
        </Switch>
    );
};

export default routes;
