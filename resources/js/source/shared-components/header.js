import React, { useState, useEffect, useRef } from 'react';
import classnames from 'classnames'
import { useHistory } from 'react-router';
import { useDispatch } from "react-redux";

/* UI Components */
import Nav from './nav';
import Basket from '../ui-components/basket/basket-component';

/* Cookie Service */
import { removeCookie } from "../services/cookie/cookieService";

/* Redux Actions */
import { logout } from '../services/auth/authService';


const navData = [
    {
        'name': 'Build Single Bottles',
        'url': '/build_single_bottles'
    }, {
        'name': 'Build Cases',
        'url': '/build_cases'
    }, {
        'name': 'Build Campaigns',
        'url': '/build_campaigns'
    }
]
const Header = () => {
    const [isScrolled, setScrolled] = useState(false);
    const ref = useRef(null);
    const history = useHistory();
    const dispatch = useDispatch();

    /* handle sticky navigation */
    /*const handleStickyNav = () => {
        const offset = window.scrollY;
        const navBarHeight = ref.current.clientHeight;
        if (offset > navBarHeight ) {
            setScrolled(true);
        } else {
            setScrolled(false);
        }
    }*/

    useEffect(()=> {
        //window.addEventListener('scroll', handleStickyNav);
    });

    /* handle logout button */
    const handleLogout = (e) => {
      e.preventDefault();
      removeCookie('access_token')
      dispatch(logout)
      history.push('/');
    }
    return (
        <div className={classnames('container-fluid', {'is-scrolled': isScrolled})} ref={ref}>
            <div className="header">
                <div className="logo-wrapper">Automation</div>
                <Nav navs={ navData } />
                <div className="header-right">
                  <Basket />
                  <a href="#" className="button button-logout" onClick={ handleLogout }><span className="button-logout-label">Logout</span> </a>
                </div>
            </div>
        </div>
    );
};

export default Header;
