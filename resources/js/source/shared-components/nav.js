import React from 'react';
import { NavLink } from 'react-router-dom';

const Nav = ({ navs, handleLogout }) => {
    const navList = navs.map(nav => {
        return (<NavLink key={ nav.name } to={ nav.url } target={ nav.target } className="nav-link" activeClassName="active" onClick={ handleLogout }>{ nav.name }</NavLink>)
    })
    return (
        <div className="nav">
            <div className="nav-bar">
                { navList }
            </div>
        </div>
    );
};

export default Nav;
