import React from 'react';
import classnames from 'classnames';

const FlashMessage = (props) => {
    const {type, message} = props.message;
    return (
        <div className={classnames('alert', {
            'alert-success': type === 'success',
            'alert-danger': type === 'error'
        })}>
            {message}
        </div>
    );
};

export default FlashMessage;
