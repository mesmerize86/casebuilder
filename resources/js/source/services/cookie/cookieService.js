import React from 'react';
import Cookies from 'universal-cookie';

const cookie = new Cookies();

/**
 *
 * @param key   => string
 * @returns {*}
 */
const getCookie = (key)=> {
    return cookie.get(key);
}

/**
 *
 * @param key     => string
 * @param value   => string
 * @param options => object
 */
const setCookie = (key, value, options)=> {
    cookie.set(key, value, options);
}

/**
 *
 * @param key => string
 */
const removeCookie = (key)=> {
    cookie.remove(key);
}

export {
    getCookie,
    setCookie,
    removeCookie
}
