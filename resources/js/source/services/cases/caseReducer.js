import {GET_CASE_BY_CASE_CODE_SUCCESS, GET_CASE_BY_CASE_CODE_ERROR, GET_CASE_BY_CASE_CODE_LOADING } from './types';

export default (state = {}, action = {}) => {
    switch(action.type) {
        case GET_CASE_BY_CASE_CODE_LOADING :
            return {...state, isLoading: true, selectedCaseCode: action.payload }
        case GET_CASE_BY_CASE_CODE_SUCCESS :
            return { ...state, caseByCaseCode:action.payload, isLoading: false }
        case GET_CASE_BY_CASE_CODE_ERROR :
            return { ...state, caseByCaseCode:action.payload, isLoading: false }
        default :
            return state
    }
}
