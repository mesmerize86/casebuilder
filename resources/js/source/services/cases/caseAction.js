import {GET_CASE_BY_CASE_CODE_SUCCESS, GET_CASE_BY_CASE_CODE_ERROR , GET_CASE_BY_CASE_CODE_LOADING} from './types';
import { addToBasket } from '../basket/basketAction';

import axios from 'axios';
const COUNT = 1;

/**
 * get the casecode and index vaule of selected component
 *
 *
 * @param caseCode //case code
 * @param index //index value to selected component
 * @returns {function(...[*]=)}
 */
export const getCaseByCaseCode = (caseCode, index) => {
    return (dispatch) => {
        dispatch({ type: GET_CASE_BY_CASE_CODE_LOADING , payload: index });
        axios.get('/api/cases/' + caseCode)
            .then(response => {
                dispatch({ type: GET_CASE_BY_CASE_CODE_SUCCESS, payload: response.data });
                let basketItem = {
                    count: COUNT,
                    items: [response.data]
                }
                dispatch(addToBasket(basketItem));
            })
            .catch(error => {
                dispatch({ type: GET_CASE_BY_CASE_CODE_ERROR, payload: error.response.data.error  })
            })
    }
}
