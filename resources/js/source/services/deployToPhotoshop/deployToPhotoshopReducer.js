import {DEPLOY_TO_PHOTOSHOP_SUCCESS, DEPLOY_TO_PHOTOSHOP_LOADING, DEPLOY_TO_PHOTOSHOP_ERROR } from './types';

export default (state = {}, action = {}) => {
    switch(action.type) {
        case DEPLOY_TO_PHOTOSHOP_LOADING :
            return {...state, isLoading: true, selectedCaseCode: action.payload }
        case DEPLOY_TO_PHOTOSHOP_SUCCESS :
            return { ...state, caseByCaseCode:action.payload, isLoading: false }
        case DEPLOY_TO_PHOTOSHOP_ERROR :
            return { ...state, caseByCaseCode:action.payload, isLoading: false }
        default :
            return state
    }
}
