import {DEPLOY_TO_PHOTOSHOP_SUCCESS, DEPLOY_TO_PHOTOSHOP_ERROR , DEPLOY_TO_PHOTOSHOP_LOADING} from './types';

import axios from 'axios';
const COUNT = 1;

/**
 * post case contents to deploy to photoshop
 *
 *
 * @param {object} basketItems
 * @returns {function(...[*]=)}
 */
export const deployToPhotoshop = (basketItems) => {
    return (dispatch) => {
        dispatch({ type: DEPLOY_TO_PHOTOSHOP_LOADING });
        axios.post('/api/deployToPhotoshop', basketItems )
            .then(response => {
                dispatch({ type: DEPLOY_TO_PHOTOSHOP_SUCCESS, payload: response.data });
            })
            .catch(error => {
                dispatch({ type: DEPLOY_TO_PHOTOSHOP_ERROR, payload: error.response.data.error  })
            })
    }
}
