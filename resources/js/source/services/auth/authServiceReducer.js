import {AUTHENTICATE_USER_SUCCESS, AUTHENTICATE_USER_ERROR, LOGOUT_USER } from './types';

export default (state = {}, action = {}) => {
    switch(action.type) {
      case AUTHENTICATE_USER_SUCCESS :
          return { ...state, credentials :action.payload }
      case AUTHENTICATE_USER_ERROR :
          return { ...state, credentials:action.payload }
      case LOGOUT_USER :
        return { credentials: {} }
      default :
          return state
    }
}
