import axios from 'axios';
import {AUTHENTICATE_USER_SUCCESS, AUTHENTICATE_USER_ERROR, AUTHENTICATE_USER_LOADING , LOGOUT_USER} from './types';
import { setCookie } from "../cookie/cookieService";

const expiresAt = 60 * 24;

/**
 *  validate user credential
 * @param credentials {} => credential
 * @returns {function(*): Promise<void>}
 * @constructor
 */
export const AuthService = (credentials)=>{
    return async function authenticateUser(dispatch) {
        dispatch({ type: AUTHENTICATE_USER_LOADING, payload: 'isLoading'});
        try {
            const response = await axios.post('/api/login', credentials);
            dispatch({ type: AUTHENTICATE_USER_SUCCESS, payload: response.data});
        } catch(error) {
            dispatch({ type: AUTHENTICATE_USER_ERROR, payload: error.response})
        }
    }
}

/**
 * remove credentials from redux store
 *
 * @param dispatch
 */
export const logout = (dispatch) => {
  dispatch({ type:LOGOUT_USER });
}

/**
 * set a cookie if customer tick on remember checkbox
 *
 * @param response => auth token
 * @param remember => remember me flag
 * @returns {boolean}
 */
export const handleLoginSuccess = (response, remember)=> {

    if (!remember) {
        const options = { path: '/'};
        setCookie('access_token', response.access_token, options);
        return true;
    }

    let date = new Date();
    date.setTime(date.getTime() + (expiresAt * 60 * 1000));
    const options = { path: '/', expires: date };
    setCookie('access_token', response.access_token, options);
    return true;
}
