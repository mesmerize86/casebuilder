import {ADD_TO_BASKET_SUCCESS, ADD_TO_BASKET_ERROR, UPDATE_BASKET } from './types';
let initialState = {
    count: 0,
    items: []
}

export default (state = initialState, action = {}) => {
    switch(action.type) {
        case ADD_TO_BASKET_SUCCESS :
            state.count = state.count + action.payload.count;
            return {...state, count: state.count, items: [...state.items, ...action.payload.items]}
        case ADD_TO_BASKET_ERROR :
            return { ...state, initialState: action.payload }
        case UPDATE_BASKET :
            state.count = state.count - 1;
            state.items.splice(action.payload, 1)
            return {...state }
        default :
            return state
    }
}
