import { ADD_TO_BASKET_SUCCESS, ADD_TO_BASKET_ERROR, UPDATE_BASKET } from './types';

/* add item to basket */
export const addToBasket = (basketItem) => {
    return (dispatch) => {
        dispatch({ type: ADD_TO_BASKET_SUCCESS, payload: basketItem})
    }
}

/* remove item from basket */
export const updateBasket = (index) => {
    return (dispatch) => {
        dispatch({ type: UPDATE_BASKET, payload: index})
    }
}
