import { GET_CAMPAIGN_BY_CAMPAIGN_CODE_SUCCESS, GET_CAMPAIGN_BY_CAMPAIGN_CODE_ERROR } from './types';
import axios from 'axios';

export const getCampaignByCampaignCode = (campaignCode) => {
        return (dispatch) => (

            axios.get('/api/campaigns/' + campaignCode)
                .then(response => {
                    dispatch({ type:GET_CAMPAIGN_BY_CAMPAIGN_CODE_SUCCESS, payload: response.data })
                  console.log('campaign responsetype', typeof response.data);
                })
                .catch(error => {
                    dispatch({ type: GET_CAMPAIGN_BY_CAMPAIGN_CODE_ERROR, payload: error.response.data.error })
                })
    );
}
