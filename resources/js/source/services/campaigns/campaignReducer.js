import { GET_CAMPAIGN_BY_CAMPAIGN_CODE_SUCCESS, GET_CAMPAIGN_BY_CAMPAIGN_CODE_ERROR } from './types';

export default (state = [], action = {}) => {
    switch(action.type) {
        case GET_CAMPAIGN_BY_CAMPAIGN_CODE_SUCCESS :
            return {...state, campaignByCampaignCode: action.payload }
        case GET_CAMPAIGN_BY_CAMPAIGN_CODE_ERROR :
            return {...state, campaignByCampaignCode: action.payload }
        default: return state
    }
}
