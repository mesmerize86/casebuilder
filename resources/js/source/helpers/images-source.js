export const defaultPath = '/images/';

export const imageNotFound = {
    'imagePath' : defaultPath,
    'imageName' : 'image-not-available.png'
}
