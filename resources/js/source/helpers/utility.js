import { defaultPath, imageNotFound} from "./images-source";

/**
 *  get an image path as a parameter. If there is no image, replace with image not found.
 *
 * @param imageSource => image source attribute
 * @returns {string}
 */
export const handleImageError = (imageSource) => {
    return ( typeof imageSource === 'undefined' || imageSource === null || imageSource === '' ) ?
        `${imageNotFound.imagePath}${imageNotFound.imageName}` : `${defaultPath}${imageSource}`
}
