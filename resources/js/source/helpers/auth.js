import React from 'react';
import { getCookie, removeCookie } from "../services/cookie/cookieService";

/* check if the user is authenticated */
const isAuthenticated = ()=> {
    let isLoggedIn = !!getCookie('access_token');
    return isLoggedIn;
}

/* when you logout remove access token cookie */
const logout = ()=> {
    removeCookie('access_token');
}

export {
    isAuthenticated,
    logout
}
