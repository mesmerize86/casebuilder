import React from 'react';

const Home = () => {
    return (
        <div className="section section-background-plain">
            <h1>Home</h1>
        </div>
    );
};

export default Home;
