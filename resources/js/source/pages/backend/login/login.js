import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';

/* ui components */
import { FloatingLabelMolecule, CheckboxMolecule, Button } from '../../../ui/molecules/form-component';

/* actions */
import { handleLoginSuccess, AuthService } from '../../../services/auth/authService';

const Login = () => {
    const {register, handleSubmit} = useForm();
    const [ isRememberMeChecked, setIsRememberMeChecked ] = useState(false);
    const history = useHistory();
    const dispatch = useDispatch();

    /* subscribe redux store*/
    const credential = useSelector(state => state.authService);

    useEffect(()=> {
        if (credential.credentials && credential.credentials.access_token) {
            handleLoginSuccess(credential.credentials, isRememberMeChecked);
            history.push('/build_campaigns');
        }
    })

    //click event
    //submit login form
    const onSubmit = (data)=> {
        const credential = {
            username: data.username,
            password: data.password
        }
        dispatch(AuthService(credential));
    }

    /* handle remember me checkbox */
    const handleRememberMe = (e)=> {
        setIsRememberMeChecked(e.target.checked);
    }
    return (
        <div className="pg-login">
            <h1 className="logo-header">Automation</h1>
            <form action="#" className="login-form" onSubmit={ handleSubmit(onSubmit) }>
                <FloatingLabelMolecule type="text" field="Email :" name="username" htmlFor="login_username" register={register}/>
                <FloatingLabelMolecule type="password" field="Password :" name="password" htmlFor="login_password" register={register}/>
                <CheckboxMolecule name="rememberme" label="Remember me" handleChange={handleRememberMe} isChecked={ isRememberMeChecked }/>
                <Button type="submit" value="Sign in" buttonType="highlight" extraClass="float-right"/>
            </form>
        </div>
    );
};

export default Login;
