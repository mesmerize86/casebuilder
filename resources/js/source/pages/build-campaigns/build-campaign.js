import React, {Component} from 'react';
import { connect, useDispatch } from 'react-redux';

/* Actions */
import { getCampaignByCampaignCode } from "../../services/campaigns/campaignAction";

/* components */
import CaseComponent from '../../ui-components/case-component/case-component';
import Form from './build-campaign-form';

const BuildCampaign = (props)=> {
    //click events:
    //handle search campaign code
    const handleSearchSubmit = (data)=> {
        props.getCampaignByCampaignCode(data.campaignCode);
    };

    return (
        <div className="pg-build-campaigns">
            <div className="section section-background-plain">
                <div className="container">
                    <Form handleSearchSubmit={handleSearchSubmit}/>
                </div>
            </div>
            { props.campaign && props.campaign.success && <CaseComponent campaignContents={props.campaign.campaign.campaignContents}/> }
        </div>
    )
}

const mapStateToProps = (state) => ({
    campaign : state.campaigns.campaignByCampaignCode,
    case: state.cases,
    basket: state.basket
});



export default connect(mapStateToProps, { getCampaignByCampaignCode })(BuildCampaign);



