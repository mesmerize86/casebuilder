import React from 'react';
import { useForm } from 'react-hook-form';
import FlashMessage from '../../shared-components/flash-message';
import { Button } from '../../ui/molecules/form-component';

const BuildCampaignForm = (props) => {
    const {register, handleSubmit, errors} = useForm();
    /* validation error type configuration*/
    const campaignCodeValidate = {
        required: true,
        pattern: /^[a-zA-Z0-9_.-]*$/
    }

    /* validation error message configuration */
    const campaignCodeValidateMessage = {
        required : {
            type: 'error',
            message: 'Please enter campaign code.'
        },
        pattern : {
            type: 'error',
            message : 'Please enter valid campaign code.'
        },
        campaignNotFound: {
            type: 'error',
            message: (props.campaign && props.campaign.message)
        }
    }

    /* form submit */
    const onSubmit = (data) => {
        props.handleSearchSubmit(data);
    }

    return (
        <form className="form-build-campaigns" onSubmit={ handleSubmit(onSubmit) }>
            <div className="form-horizontal">
                <div className="form-group">
                    <input type="text" className="form-control" name="campaignCode" placeholder="Please enter campaign code e.g. M021542" ref={register(campaignCodeValidate)}/>
                    <Button type="submit" value="Search" buttonType="highlight" />
                    {
                        errors.campaignCode &&
                        errors.campaignCode.type === "required" &&
                        <FlashMessage message={campaignCodeValidateMessage.required}/>
                    }
                    {
                        errors.campaignCode &&
                        errors.campaignCode.type === "pattern" &&
                        <FlashMessage message={campaignCodeValidateMessage.pattern}/>
                    }
                    {
                        !errors.campaignCode &&
                        props.campaign &&
                        props.campaign.status === 0 &&
                        <FlashMessage message={campaignCodeValidateMessage.campaignNotFound}/>
                    }
                </div>
            </div>
        </form>
    );
};

export default BuildCampaignForm;
