import React from 'react';

const BuildCases = () => {
    return (
        <div className="pg-build-cases">
            <div className="section section-background-plain">
                <div className="container">
                    <form className="form-build-cases">
                        <div className="form-horizontal">
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Please enter case code e.g. M021542"/>
                                <input type="submit" className="button button-secondary" value="Search"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default BuildCases;
