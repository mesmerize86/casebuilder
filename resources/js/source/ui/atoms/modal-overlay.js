import React, { useState, useEffect } from 'react';
import { componentWillAppendToBody } from 'react-append-to-body';
import classnames from 'classnames';

/**
 * if you click on any popup modal, we'll append it to body.
 * Next time we'll use show/hide for background overlay
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const ModalOverlayComponent = ({ isModalOpen, onClose }) => {

  /**
   * close popup modal when you click on overlay
   *
   * @param e => click event
   */
  const handleCloseModalOnOverlay = (e)=> {
    e.preventDefault();
    if (onClose) {
      onClose();
    }
  }
  return (
    <div className={classnames("modal-overlay", { 'modal-is-open': isModalOpen })} onClick={ handleCloseModalOnOverlay }></div>
  );
};

const ModalOverlay = componentWillAppendToBody(ModalOverlayComponent);

export default ModalOverlay;
