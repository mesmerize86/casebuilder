import React from 'react';

const button = ({ type, value, buttonType, link = false, href, target, extraClass = ''}) => {
    let buttonTypeClass;

    if (buttonType === 'primary') {
        buttonTypeClass = 'button-primary';
    } else if ( buttonType === 'highlight') {
        buttonTypeClass = 'button-highlight'
    } else if (buttonType === 'secondary') {
        buttonTypeClass = 'button-secondary';
    } else if (buttonType === 'danger') {
        buttonTypeClass = 'button-danger'
    }
    else {
        buttonTypeClass = 'button-default'
    }

    const handlePreventDefault = (e)=> {
        e.preventDefault();
    }

    const ButtonView = ()=> {
        return ( (link) ? <a href={ href || "#"} target={ target || "_blank"} className={"button " + buttonTypeClass + ' ' +    extraClass } onClick={handlePreventDefault}>{ value }</a> :
            <input type={ type || 'submit'} value={value} className={"button " + buttonTypeClass + ' ' + extraClass}/> )
    };

    return (
        <ButtonView />
    )
};

export default button;
