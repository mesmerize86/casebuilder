import React from 'react';

const Checkbox = ({isChecked, name, extraClass = '', handleChange}) => {
    return (
        <input type="checkbox" id={ name } name={ name } className={"checkbox-input" + extraClass} checked={ isChecked } onChange={ handleChange }/>
    );
};

export default Checkbox;
