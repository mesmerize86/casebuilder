import React, { useState } from 'react';
import classnames from 'classnames';

/**
 *
 * @param field           = mandatory => field name
 * @param type            = optional  => by default text if not defined
 * @param length          = optional  => max length
 * @param htmlFor         = mandatory => id name to make relationship between input and label
 * @returns {JSX.Element}
 * @constructor
 */
const FloatingLabelMolecule = ({ field, type, name, length, htmlFor, register}) => {
    const [inputHasContent, setInputHasContent] = useState(false);

    const handleChange = (e)=> {
        (e.target.value.length > 0) ? setInputHasContent(true) :  setInputHasContent(false)
    }
    return (
        <div className="floating-label-wrapper">
            <input
                type = { type || 'text'}
                id = { htmlFor }
                name = { name }
                maxLength = { length }
                onChange={handleChange}
                className={classnames({'input-has-content' : inputHasContent})}
                ref = {register } />
            <label htmlFor={ htmlFor }>
                <span className="floating-label-text">{field}</span>
            </label>
        </div>
    );
};

export default FloatingLabelMolecule;
