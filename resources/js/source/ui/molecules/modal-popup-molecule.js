import React from 'react';
import ModalOverlay from "../atoms/modal-overlay";
import classname from 'classnames';

/**
 * Modal popup molecule
 *
 *
 * @param isModalOpen       => boolean to open modal popup
 * @param handleCloseModal  => close modal event
 * @param children          => content that goes in modal
 * @returns {JSX.Element}
 * @constructor
 */
const ModalPopupMolecule = ({ isModalOpen, handleCloseModal, children }) => {
  return (
    <div className={classname("modal-content", {'modal-content-open': isModalOpen })}>
      { children }
      <ModalOverlay onClose={ handleCloseModal } isModalOpen={ isModalOpen } />
    </div>
  );
};

export default ModalPopupMolecule;
