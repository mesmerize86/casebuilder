import React from 'react';
import Checkbox from "../atoms/checkbox";

/**
 *
 * @param name            => checkbox name
 * @param label           => checkbox label
 * @param handleChange    => change event action
 * @param isChecked       => is checkbox checked or not
 * @returns {JSX.Element}
 * @constructor
 */
const CheckboxMolecule = ({ name, label, handleChange, isChecked }) => {
    return (
        <div className="checkbox-wrapper">
            <Checkbox name={name} handleChange={handleChange} isChecked={isChecked}/>
            <label htmlFor={name} className="checkbox-label">{ label }</label>
        </div>
    );
};

export default CheckboxMolecule;
