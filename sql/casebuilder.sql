-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2020 at 03:09 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casebuilder`
--

-- --------------------------------------------------------

--
-- Table structure for table `bottles`
--

CREATE TABLE `bottles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `skuCode` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `itemCode` varchar(199) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bottles`
--

INSERT INTO `bottles` (`id`, `name`, `skuCode`, `created_at`, `updated_at`, `itemCode`) VALUES
(1, 'merlot', 'sku4760034', NULL, NULL, ''),
(2, 'shiraz', 'sku4760032', NULL, NULL, ''),
(3, 'cab merlot', 'sku4320024', NULL, NULL, ''),
(4, 'cab shiraz', 'sku4821067', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `bottle_case`
--

CREATE TABLE `bottle_case` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bottle_id` bigint(20) UNSIGNED NOT NULL,
  `case_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bottle_case`
--

INSERT INTO `bottle_case` (`id`, `bottle_id`, `case_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(15, 4, 39, NULL, NULL),
(16, 3, 39, NULL, NULL),
(17, 2, 39, NULL, NULL),
(18, 1, 39, NULL, NULL),
(19, 1, 47, NULL, NULL),
(20, 2, 47, NULL, NULL),
(21, 3, 47, NULL, NULL),
(22, 4, 47, NULL, NULL),
(23, 1, 48, NULL, NULL),
(24, 2, 48, NULL, NULL),
(25, 3, 48, NULL, NULL),
(26, 4, 48, NULL, NULL),
(27, 1, 52, NULL, NULL),
(28, 2, 52, NULL, NULL),
(29, 3, 52, NULL, NULL),
(30, 4, 52, NULL, NULL),
(31, 1, 53, NULL, NULL),
(32, 2, 53, NULL, NULL),
(33, 1, 54, NULL, NULL),
(34, 2, 54, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `campaignCode` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `name`, `campaignCode`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', '2020-08-27 01:30:59', '2020-08-27 01:30:59'),
(2, 'test2', 'test2', '2020-08-27 01:32:25', '2020-08-27 01:32:25'),
(3, 'campaign1', 'campaign1', '2020-08-27 02:32:29', '2020-08-27 02:32:29'),
(4, 'campaign2', 'campaign2', '2020-08-27 02:38:44', '2020-08-27 02:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_case`
--

CREATE TABLE `campaign_case` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` bigint(20) UNSIGNED NOT NULL,
  `case_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `campaign_case`
--

INSERT INTO `campaign_case` (`id`, `campaign_id`, `case_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL),
(3, 4, 39, NULL, NULL),
(5, 4, 47, NULL, NULL),
(6, 4, 48, NULL, NULL),
(7, 4, 47, NULL, NULL),
(8, 4, 48, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cases`
--

CREATE TABLE `cases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `caseCode` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `caseType` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `numberOfBottles` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cases`
--

INSERT INTO `cases` (`id`, `name`, `caseCode`, `caseType`, `numberOfBottles`, `created_at`, `updated_at`) VALUES
(1, 'Mixed Red', '123456', '4*3', 4, NULL, NULL),
(39, 'merlot case', '342346', '2*3', 4, '2020-08-17 02:11:51', '2020-08-17 02:11:51'),
(47, 'test 3', 'test3', '4*3', 4, '2020-08-24 01:56:40', '2020-08-24 01:56:40'),
(48, 'test 4', 'test 4', '2*6', 2, '2020-08-24 02:16:09', '2020-08-24 02:16:09'),
(52, '8', '8', '8', 12, '2020-08-27 01:53:46', '2020-08-27 01:53:46'),
(53, '9', '9', '9', 12, '2020-08-27 01:56:24', '2020-08-27 01:56:24'),
(54, '9', '91', '9', 12, '2020-08-27 01:57:04', '2020-08-27 01:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_11_111555_create_bottle_table', 1),
(5, '2020_08_11_111556_create_case_table', 1),
(6, '2020_08_15_122513_create_bottle_case_table', 1),
(7, '2020_08_24_103403_add_colmn_name_bottle', 2),
(8, '2020_08_24_103742_change_column_data_type', 3),
(9, '2020_08_27_082434_create_campaign_table', 4),
(10, '2020_08_27_082452_create_campaigns_table', 5),
(11, '2020_08_27_075928_update_campaign_column_name', 6),
(12, '2014_08_27_082510_create_campaign_case_table', 7),
(13, '2016_06_01_000001_create_oauth_auth_codes_table', 8),
(14, '2016_06_01_000002_create_oauth_access_tokens_table', 8),
(15, '2016_06_01_000003_create_oauth_refresh_tokens_table', 8),
(16, '2016_06_01_000004_create_oauth_clients_table', 8),
(17, '2016_06_01_000005_create_oauth_personal_access_clients_table', 8),
(18, '2020_12_01_033040_create_users_table', 9),
(19, '2020_12_01_033554_create_users_table', 10),
(20, '2020_12_01_033724_create_users_table', 11),
(22, '2020_12_08_104047_create_product_images_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0063442d4d4f460b72697f1fe92e26875e039ac4c8746cbfc785cff7cb6831d283561106e07928df', 1, 2, NULL, '[]', 0, '2020-12-07 00:27:26', '2020-12-07 00:27:26', '2021-12-07 11:27:26'),
('0287750e05cd76ab03c3b0d4443b73bdf45bbbc1a1e628c39309cc76d4b57ff6c41743f62d359d47', 1, 2, NULL, '[]', 0, '2020-12-02 16:58:35', '2020-12-02 16:58:35', '2021-12-03 03:58:35'),
('04998fc421fae4fe1539928660f2ee8a98f61f6aace4427fd3d1ee5d94c6765778605437543e4e70', 1, 2, NULL, '[]', 0, '2020-12-10 08:56:17', '2020-12-10 08:56:17', '2021-12-10 19:56:17'),
('0658e166582621cff07299f0bd2963a009861d8fdeb4ec81919171829e82ce665cc1f3b9b14f5bd2', 1, 2, NULL, '[]', 0, '2020-12-02 17:59:53', '2020-12-02 17:59:53', '2021-12-03 04:59:53'),
('06b216af436eb615a136d27379766d0dd544e97fa1e85621632cfd70b16546ee373677ecec864457', 1, 2, NULL, '[]', 0, '2020-11-30 18:44:19', '2020-11-30 18:44:19', '2021-12-01 05:44:19'),
('078e96323cbaf5c5a181fdcdc05a1ca6134147800c35abbc713c1750c79364b31ebc6aeec34741a1', 1, 2, NULL, '[]', 0, '2020-12-07 00:26:49', '2020-12-07 00:26:49', '2021-12-07 11:26:49'),
('0a57e9fe6e4e286ec8e973f9a10142e894150cb9343e9afc7277e88fe628fb4193ba764d16b7c933', 1, 2, NULL, '[]', 0, '2020-12-07 00:22:13', '2020-12-07 00:22:13', '2021-12-07 11:22:13'),
('0cce86bd9760f7a747db1cb3870ed411ce68a0d31eba99b292e29cec008b415e7bccbc17bbe5ca1d', 1, 2, NULL, '[]', 0, '2020-12-07 00:25:30', '2020-12-07 00:25:30', '2021-12-07 11:25:30'),
('0f19bf0ce71fd3447ea0c5553fc65b3f6b73547d762080800ec62fb9efc0306187af0517bd300f6a', 1, 2, NULL, '[]', 0, '2020-12-04 01:31:31', '2020-12-04 01:31:31', '2021-12-04 12:31:31'),
('19ac6cad659c366a5c1aba4f67f8f3f4df4aa4874922518ca2e389c9b0eb2b3ac8443cf3c9992ab7', 1, 2, NULL, '[]', 0, '2020-12-05 22:18:46', '2020-12-05 22:18:46', '2021-12-06 09:18:46'),
('1e0a6cb417b4f502d1729c27b456cf4e82091f12cba2690d92a214b3fcbe341ebb3924e9b211636c', 1, 2, NULL, '[]', 0, '2020-12-02 17:00:59', '2020-12-02 17:00:59', '2021-12-03 04:00:59'),
('27a66d05751f9ef4161b454172f644ce91e8ab79b5c6f6fee5bf786a360e3c43db22599a6f5bf2f6', 1, 2, NULL, '[]', 0, '2020-12-02 17:52:57', '2020-12-02 17:52:57', '2021-12-03 04:52:57'),
('27cc1426005c3d65edeabfc07490e105574759f95fb6902777b55c29c1e729bc66a12066a669fbdd', 1, 2, NULL, '[]', 0, '2020-12-07 00:32:35', '2020-12-07 00:32:35', '2021-12-07 11:32:35'),
('29e8e0ae17938f2da63b6ab63a102217ff19ddaa99e9e05823a008684d421cb602504ed1664b2023', 1, 2, NULL, '[]', 0, '2020-12-06 01:29:16', '2020-12-06 01:29:16', '2021-12-06 12:29:16'),
('2a2cb554610342ee37849d29acbe70894391c2934d55cdc8276c35ba8585b561e524f5d5d4f3e138', 1, 2, NULL, '[]', 0, '2020-12-09 16:34:45', '2020-12-09 16:34:45', '2021-12-10 03:34:45'),
('2bf9c28ed269cff0ce3fadc608144add2a6d210049405340a8ce73ef31bb3826b16ce5b8b9137531', 1, 2, NULL, '[]', 0, '2020-12-02 18:02:09', '2020-12-02 18:02:09', '2021-12-03 05:02:09'),
('2e9e791838700071bc2c08394d3b6d1d0ad459c9818573a24bdad0410e4343aec97b935e6d4a6de1', 1, 2, NULL, '[]', 0, '2020-12-07 00:21:09', '2020-12-07 00:21:09', '2021-12-07 11:21:09'),
('2f4207615a62060bb6fa33bc2d9df4ab1e1b0781b913917047713980cf612cb25e56262460ed99e7', 1, 2, NULL, '[]', 0, '2020-12-05 01:02:33', '2020-12-05 01:02:33', '2021-12-05 12:02:33'),
('308912ce1695cf5775f009e3deb83660d4769a362104e0f7513ac7301ff95f11ecb2e9f57de7ff34', 1, 2, NULL, '[]', 0, '2020-12-06 01:34:57', '2020-12-06 01:34:57', '2021-12-06 12:34:57'),
('3159de67b41f6241bb710b7a9ee571ccd0f809f1ae36a2c9dcd8aa6d4ea2981d24c9952a8ee96a0f', 1, 2, NULL, '[]', 0, '2020-12-09 15:30:57', '2020-12-09 15:30:57', '2021-12-10 02:30:57'),
('323614994de9202c0e51b1d8f4dd84d23f65de33d1bc0c9579b823f11e6e19d8b1d9329a7608964c', 1, 2, NULL, '[]', 0, '2020-12-02 18:01:55', '2020-12-02 18:01:55', '2021-12-03 05:01:55'),
('3349529f9d3a4d3dead3ff4ecab524eefa0797b781f5f3dc2a891f426538288f66f0e4ecdec96565', 1, 2, NULL, '[]', 0, '2020-12-10 09:22:15', '2020-12-10 09:22:15', '2021-12-10 20:22:15'),
('34aceffccdcf6af08d36896316bd4138346875dd38c1a96c6ac50dd1a4288d2c1226f2327993c78f', 1, 2, NULL, '[]', 0, '2020-12-04 01:46:24', '2020-12-04 01:46:24', '2021-12-04 12:46:24'),
('34ecc9dab8b9e5dbe5966b4c69af9c575afd35955a7968bc0c7dab9befaf8d8de4351f1eb8803a6f', 1, 2, NULL, '[]', 0, '2020-12-16 12:45:23', '2020-12-16 12:45:23', '2021-12-16 23:45:23'),
('365e0f6aa512e6d1950110b27fc33d56429d22a2d6abd2a8a01f4a4165608f39f74430ba192792ab', 1, 2, NULL, '[]', 0, '2020-12-10 09:32:58', '2020-12-10 09:32:58', '2021-12-10 20:32:58'),
('3709f2bf64583376e62a04533ceeeaa9e2ee7ff7f2a208fc4f0a0c83591a51512ecee9de50f1b032', 1, 2, NULL, '[]', 0, '2020-12-07 00:19:10', '2020-12-07 00:19:10', '2021-12-07 11:19:10'),
('386dc3ba1b3a2019779cc8a1acfbdc44d8367b645140dccb80bf67aab553c0dfc80b8069a60fb645', 1, 2, NULL, '[]', 0, '2020-12-10 09:18:50', '2020-12-10 09:18:50', '2021-12-10 20:18:50'),
('3a7b0f7e4a6800dccc43da978f181ad840fe18e22cd03164a55f98480bc496e32324d3ca4b96ef1f', 1, 2, NULL, '[]', 0, '2020-12-10 09:18:06', '2020-12-10 09:18:06', '2021-12-10 20:18:06'),
('3cfcce2604ef1c326e92b9987dbbac73360b7d7b04f1e2facdd84e3769d88041b73e1ebe75718ca4', 1, 2, NULL, '[]', 0, '2020-12-05 22:19:32', '2020-12-05 22:19:32', '2021-12-06 09:19:32'),
('3e928fd4fdb588f8269aa8da397abb4c1534fdef23384e642493f0dffb36094d4708855d9b9103ad', 1, 2, NULL, '[]', 0, '2020-12-02 17:44:31', '2020-12-02 17:44:31', '2021-12-03 04:44:31'),
('3ec128be1c6e19279c8fb69d2c68a663783cb59c9931b40caaf33cc9de58bd3d3fd15c09cb8cd052', 1, 2, NULL, '[]', 0, '2020-11-30 18:54:17', '2020-11-30 18:54:17', '2021-12-01 05:54:17'),
('405736d8cb002691d3d9066229cc3c15ad91b81d9eaccab60f5e2988f9a2aab635182ac272d4996f', 1, 2, NULL, '[]', 0, '2020-12-04 10:53:37', '2020-12-04 10:53:37', '2021-12-04 21:53:37'),
('42e36fddfa28d55e8215b3b59adba6b02d413f9fdb83ee72c83b01adb217ad323928ac1742ab6067', 1, 2, NULL, '[]', 0, '2020-12-06 02:03:34', '2020-12-06 02:03:34', '2021-12-06 13:03:34'),
('446460bf50f4aa5f5c970d7866fead2a082bad79510ac6cc2b98b06a823183c186dbdbbe58e2aba1', 1, 2, NULL, '[]', 0, '2020-12-02 16:53:13', '2020-12-02 16:53:13', '2021-12-03 03:53:13'),
('4624df60e4b5386bde553bdbe77a0951c9009ebabe3d8c0d48fd48f50c859e5e4729e3b1a5d90e34', 1, 2, NULL, '[]', 0, '2020-12-08 09:02:57', '2020-12-08 09:02:57', '2021-12-08 20:02:57'),
('471aee99b15bf82810d16c8f8d3b14e0fe1b43e6b36f612550032df77c3cbce6e5787075ff904c8c', 1, 2, NULL, '[]', 0, '2020-12-16 12:39:28', '2020-12-16 12:39:28', '2021-12-16 23:39:28'),
('485b3ec909c13ad8fa67fc08fd028b7605295cf625c0b4f0a1f1a8b3d6f5a98e7082e7c492cedc68', 1, 2, NULL, '[]', 0, '2020-12-12 23:56:40', '2020-12-12 23:56:40', '2021-12-13 10:56:40'),
('4870a3d9327db68ff9280ae4fd9fae86ec74741a37adb0edf78db48af229b42f8c2c5bfc487e4735', 1, 2, NULL, '[]', 0, '2020-11-30 17:26:50', '2020-11-30 17:26:50', '2021-12-01 04:26:50'),
('4e6985626e39fa8d8133893c72963e7cae3b7f4fcf66bab1831c46cd70313abb86a2e8ae9e495bcc', 1, 2, NULL, '[]', 0, '2020-12-06 02:01:45', '2020-12-06 02:01:45', '2021-12-06 13:01:45'),
('4f3ad69bbe32fa2f9499e9b14198374f0bdd60467e5bd8d2c0e4c5363ec79c6b85b55a02ea47b5a5', 1, 2, NULL, '[]', 0, '2020-12-10 09:44:34', '2020-12-10 09:44:34', '2021-12-10 20:44:34'),
('52fa52aa4d7f8193abf40297b8eff8a2f357a9b049e83d46774620836eac1be82f120813b64b0977', 1, 2, NULL, '[]', 0, '2020-12-05 21:51:05', '2020-12-05 21:51:05', '2021-12-06 08:51:05'),
('5405a158e333c396e390292c25be1a935a6f18a4942eb300ca08e85865e0174a32dc411d525aa54a', 1, 2, NULL, '[]', 0, '2020-12-10 08:51:27', '2020-12-10 08:51:27', '2021-12-10 19:51:27'),
('56e7892a51dc43c458ba9eb003448c23e7dedf1272e12f02c67cc1c2b96790533f6cbdb4ab7b3515', 1, 2, NULL, '[]', 0, '2020-12-01 10:09:05', '2020-12-01 10:09:05', '2021-12-01 21:09:05'),
('579cffd4427f53f8df51beb60f2cb0fc16ddf72e9f67397a19a06bc3f26a0c8784750a5e0135c89c', 1, 2, NULL, '[]', 0, '2020-12-01 08:58:21', '2020-12-01 08:58:21', '2021-12-01 19:58:21'),
('59331916bf4074783abca90f2df21856dcaf7ab2d6cfa2d76de7855095fede3e42d3fec23a65cd74', 1, 2, NULL, '[]', 0, '2020-12-10 08:31:55', '2020-12-10 08:31:55', '2021-12-10 19:31:55'),
('5ba5623e0e5ddf1fec1ce70480daee6ad9f3ce957c777880e674e4987364bc251e93bd05dcc07c67', 1, 2, NULL, '[]', 0, '2020-12-01 00:20:59', '2020-12-01 00:20:59', '2021-12-01 11:20:59'),
('5d76578f0b16230eba760470a15bf5ddb4bcc3285cc72b1448b4bad5109d6124d6bcd212b5afff54', 1, 2, NULL, '[]', 0, '2020-12-02 17:58:20', '2020-12-02 17:58:20', '2021-12-03 04:58:20'),
('5e5189c0336a1773d857ac33959ab3687fdf94c4c1cca76e871b2f721ca234b716019c4cdfdc0f8c', 1, 2, NULL, '[]', 0, '2020-12-02 18:00:22', '2020-12-02 18:00:22', '2021-12-03 05:00:22'),
('63cd5f849b75c1c8f7fa802700517933ecc7c521fa0ff5e7c0e3a84a5451f2e9b1588f60e0c9bf79', 1, 2, NULL, '[]', 0, '2020-12-01 10:07:20', '2020-12-01 10:07:20', '2021-12-01 21:07:20'),
('648a7515c705ecb2517ea347d728b7b80a6bcd5bd801846efb653090a3b04116a63cbe337a336b9e', 1, 2, NULL, '[]', 0, '2020-12-01 08:54:25', '2020-12-01 08:54:25', '2021-12-01 19:54:25'),
('6922681680f35d97e3b8c05b0f6d1748c7de97c0c26186d53c568bdbceda8786efbee3ceb72a58f3', 1, 2, NULL, '[]', 0, '2020-12-10 00:29:57', '2020-12-10 00:29:57', '2021-12-10 11:29:57'),
('6a1187177a4b720a9d5bb56afe5cfef20473061bd080021921306cc0646a774b4bd7d07165c0539c', 1, 2, NULL, '[]', 0, '2020-12-02 23:20:27', '2020-12-02 23:20:27', '2021-12-03 10:20:27'),
('6d972c9bb738ab340cc67ea59204f39e6db0ec61bb3bb76c05315ab01228733fa2119328eff271a2', 1, 2, NULL, '[]', 0, '2020-12-17 03:27:49', '2020-12-17 03:27:49', '2021-12-17 14:27:49'),
('6dc3c42947b60c06681302ac561819bf6f6840d9bb1c84abd49440d0f32238b7587d7cc905b8dfa4', 1, 2, NULL, '[]', 0, '2020-12-06 02:07:50', '2020-12-06 02:07:50', '2021-12-06 13:07:50'),
('6e9cfc190e569e45c8eed09c863ce982df7ac89c94396aa609b51874d787ce1c436a4fdc41709dc2', 1, 2, NULL, '[]', 0, '2020-12-02 18:00:43', '2020-12-02 18:00:43', '2021-12-03 05:00:43'),
('75a70cbe337ed7007cf5d48a77cf3800e5bf047e7c080d6326be31085dce4f2cae77239183397919', 1, 2, NULL, '[]', 0, '2020-12-02 16:57:38', '2020-12-02 16:57:38', '2021-12-03 03:57:38'),
('762304e5139e17fc5f558b175e437823df8e5c55ebd988f3a64285dc7d7dfef926f41da92e954fc5', 1, 2, NULL, '[]', 0, '2020-12-01 10:10:58', '2020-12-01 10:10:58', '2021-12-01 21:10:58'),
('77f3d473a53cf138d28af0a1a5134fe7c880bd4f9b650196a3f6d112dc559666b454192cc8c7211b', 1, 2, NULL, '[]', 0, '2020-12-06 01:41:26', '2020-12-06 01:41:26', '2021-12-06 12:41:26'),
('7ad5aee633e68d1d28f18b73b808cec7ad4c5fa1d2cc5e2593c948c09fa4a3b0ae8228a95c858821', 1, 2, NULL, '[]', 0, '2020-12-07 22:27:32', '2020-12-07 22:27:32', '2021-12-08 09:27:32'),
('7c2f4661eb67ef9cc8840e11a9c121f76bb303bdfdbfa4ef80737bce43181d05f1ca689e3c8044b5', 1, 2, NULL, '[]', 0, '2020-12-10 08:32:48', '2020-12-10 08:32:48', '2021-12-10 19:32:48'),
('844f14067a8b8d742adc70fbde921638f8d1649d75677a362c10bbea987f6ac1c1012c5e10295d10', 1, 2, NULL, '[]', 0, '2020-12-02 17:59:45', '2020-12-02 17:59:45', '2021-12-03 04:59:45'),
('8528a5a57f3926aab58812881f6be2069d2a193477503a30109e38ea3e45a73e82a297aba952a33b', 1, 2, NULL, '[]', 0, '2020-12-02 18:01:25', '2020-12-02 18:01:25', '2021-12-03 05:01:25'),
('853b16d767bfabf6c518e028a9fbdf13ac80014b7d9e9150328cbd2fd68fb7b74fcc9fd20f71c03d', 1, 2, NULL, '[]', 0, '2020-12-10 08:47:42', '2020-12-10 08:47:42', '2021-12-10 19:47:42'),
('8b99fbbc4ddc7f43131353b8d7fe1bab934fa5687324af6a7efebab27debade2db5fb27c990e3a00', 1, 2, NULL, '[]', 0, '2020-12-01 10:06:16', '2020-12-01 10:06:16', '2021-12-01 21:06:16'),
('8d1d07b40d5e511a4d7c5f2f6c63f18fcc1cbcb29e079665606aa8edd3ba706075c7af72da0bb55b', 1, 2, NULL, '[]', 0, '2020-12-15 00:37:00', '2020-12-15 00:37:00', '2021-12-15 11:37:00'),
('8ecd31976079a4685dce46dee9f634700beebff5fcba8da491f5e6a9ac767d85d0c3e29c1f2d1441', 1, 2, NULL, '[]', 0, '2020-12-02 17:57:18', '2020-12-02 17:57:18', '2021-12-03 04:57:18'),
('903b99f7b14f527525e4522b6f932175700162d6102ab07d2119629a7d546a54a928cec0598fb901', 1, 2, NULL, '[]', 0, '2020-12-10 00:16:39', '2020-12-10 00:16:39', '2021-12-10 11:16:39'),
('93dd53db1f323a20a043c07a8c9e63d3e5a9cba53742f9f9cdcdfb48304dca7075d45061d8700d06', 1, 2, NULL, '[]', 0, '2020-12-14 00:10:53', '2020-12-14 00:10:53', '2021-12-14 11:10:53'),
('95492b34fb05d27422da944b27293743f1ac5ed6c8a41ceaa54786afcd25ebf110b58b58970d10c4', 1, 2, NULL, '[]', 0, '2020-12-05 03:13:27', '2020-12-05 03:13:27', '2021-12-05 14:13:27'),
('9a1d0fe9b9d6586589f0706fadcffb92528a3e041519cfc7c98ad44b9c39f0db394b96e2452a664e', 1, 2, NULL, '[]', 0, '2020-12-16 12:39:26', '2020-12-16 12:39:26', '2021-12-16 23:39:26'),
('9bc0096ea1459edde426f039f9fbd6efb18954a90b0352c4c1051913b1bdbde5bab4552b7522a31c', 1, 2, NULL, '[]', 0, '2020-12-07 00:04:15', '2020-12-07 00:04:15', '2021-12-07 11:04:15'),
('9cd2ee72f53fea095c3a944abb925bae101ba6a1ba8023d94e99044d9d026de4fee53fffa2c5020d', 1, 2, NULL, '[]', 0, '2020-12-09 15:04:41', '2020-12-09 15:04:41', '2021-12-10 02:04:41'),
('9e2d5eee983856ea26abce8c21c43945f781e62a80885444ae90b9b162162b13c5dbb9d84562e82e', 1, 2, NULL, '[]', 0, '2020-12-10 08:50:03', '2020-12-10 08:50:03', '2021-12-10 19:50:03'),
('9f93f2c0475a0d4019af836c3eca14024850b1fdb4e4c6e230264c67397a898e985df251cff02335', 1, 2, NULL, '[]', 0, '2020-12-04 01:30:04', '2020-12-04 01:30:04', '2021-12-04 12:30:04'),
('9fde9d6d46a7561ff337f772dd636cfa5a2eee081daef3f17d8fc5a9294a4ed342481942a4d1b45d', 1, 2, NULL, '[]', 0, '2020-12-16 12:48:42', '2020-12-16 12:48:42', '2021-12-16 23:48:42'),
('a14de0dc95b1e78125b6701214417d4bf223b5e1724ac81c60dc9ad66df3714fbcebb2755d7dae05', 1, 2, NULL, '[]', 0, '2020-12-04 10:39:09', '2020-12-04 10:39:09', '2021-12-04 21:39:09'),
('a17b240b5964f9ba1c7b6341301cae1897df4ea7cca1d3466208ede27dffb9cabd587fe3a8debf2c', 1, 2, NULL, '[]', 0, '2020-12-04 10:45:08', '2020-12-04 10:45:08', '2021-12-04 21:45:08'),
('a46236845b7a4e49e2eb9d164d13aa0a969f0b62494935f4bf292ec7b8ac719dffba8da63eb65039', 1, 2, NULL, '[]', 0, '2020-12-04 10:43:58', '2020-12-04 10:43:58', '2021-12-04 21:43:58'),
('a9db58f63ed6f1e3977d4ce526908529ea6aa4f097c2b422a7ab7e9f52ca62478cd32dc15b6c0579', 1, 2, NULL, '[]', 0, '2020-12-06 02:04:34', '2020-12-06 02:04:34', '2021-12-06 13:04:34'),
('aa69f918a58c039512292d6cfdd69f45ab14c43918021abb4a6903d858aab971fb1c443af58414ba', 1, 2, NULL, '[]', 0, '2020-12-02 17:37:28', '2020-12-02 17:37:28', '2021-12-03 04:37:28'),
('ab1fdaeddaf6434f6f088e472d306a527575953eecb2e86fd2ac8b2210e40dab0cb153dd93a972e3', 1, 2, NULL, '[]', 0, '2020-12-06 01:50:30', '2020-12-06 01:50:30', '2021-12-06 12:50:30'),
('ac3fe22d661b94a92fde394cee15bba290e50bc23799ceeda864c9992e7d5924f3106ac82f5db272', 1, 2, NULL, '[]', 0, '2020-12-04 10:53:36', '2020-12-04 10:53:36', '2021-12-04 21:53:36'),
('acbd95f8920cf16235323951e47c749b7bed924f2b5052ef14748041ead842a9a43436cfe34e4be5', 1, 2, NULL, '[]', 0, '2020-12-07 10:04:53', '2020-12-07 10:04:53', '2021-12-07 21:04:53'),
('ad13ddea1b6d68d8d6c756f08f6dac004a603bc3c67029ac131e19f6f87a6785bfd18c7a13c5140e', 1, 2, NULL, '[]', 0, '2020-11-30 19:01:09', '2020-11-30 19:01:09', '2021-12-01 06:01:09'),
('ad70cbca5db910fba25fc695163ab6c7a6d9f8bb05b2d355273c2ff571258afbd690354bb6b76914', 1, 2, NULL, '[]', 0, '2020-12-10 08:27:14', '2020-12-10 08:27:14', '2021-12-10 19:27:14'),
('b1fe7f207825b3c584b9eb66293d5d130a99102f81eff0ec127c38c7261ee5d6215c8ed164fca362', 1, 2, NULL, '[]', 0, '2020-12-02 17:55:58', '2020-12-02 17:55:58', '2021-12-03 04:55:58'),
('b2f68bb4f7effff6c879c037714fc8b5c1aaa0ad17365e1bd5c883fbc399cca7d1d5160e431c4739', 1, 2, NULL, '[]', 0, '2020-12-06 01:21:43', '2020-12-06 01:21:43', '2021-12-06 12:21:43'),
('b31c9dd60c731c077e63af9d6fdd071037aa0ef0e758d9b491d7d7feac5be58fc99f137f8b332769', 1, 2, NULL, '[]', 0, '2020-12-02 16:59:31', '2020-12-02 16:59:31', '2021-12-03 03:59:31'),
('bcbf8a8d7e602c45a365963dad58b837d6a23c5790ee44313e92b9b9537f613853853ea0e005a4fb', 1, 2, NULL, '[]', 0, '2020-12-07 00:13:49', '2020-12-07 00:13:49', '2021-12-07 11:13:49'),
('bd6efb8f07b1d23f57c458bfbd930d0bbc306b868e5e332b97ba2ede1dc64a0fba5b117466af72b8', 1, 2, NULL, '[]', 0, '2020-12-01 08:54:38', '2020-12-01 08:54:38', '2021-12-01 19:54:38'),
('be52cb0371321644152944ced2310f904f29cf6e238e7dc0d3af88394a847f20d8c850e83d3b9287', 1, 2, NULL, '[]', 0, '2020-12-06 01:35:17', '2020-12-06 01:35:17', '2021-12-06 12:35:17'),
('c19218759411d9da8a2eb39e5d439988943e71870ff7ff0d9856f481911b0c28784b8776aaa13ef1', 1, 2, NULL, '[]', 0, '2020-12-01 10:13:06', '2020-12-01 10:13:06', '2021-12-01 21:13:06'),
('c1b78f04d9f7fd1f9d77afe322db515508a0b76f122f3ddf7b02c0d4e36cb3180990c168fd1399f8', 1, 2, NULL, '[]', 0, '2020-12-10 08:33:57', '2020-12-10 08:33:57', '2021-12-10 19:33:57'),
('cb0c9a292114059cc019e27eac82b3a5c92876903583424b44cb86da4d4e9b64d7d8ddb522dfea8d', 1, 2, NULL, '[]', 0, '2020-12-07 00:00:51', '2020-12-07 00:00:51', '2021-12-07 11:00:51'),
('cb6d56194ce7c999c508adf4e6d8b9fb114c0db0aee36b2049fb6c904c24f36b1f64e8f32beff1c2', 1, 2, NULL, '[]', 0, '2020-12-06 01:43:30', '2020-12-06 01:43:30', '2021-12-06 12:43:30'),
('cf304ee769dcd790d23b51f560f82c13fa57be3f21542cc382625af04417a3a0266980cb6f0e6f82', 1, 2, NULL, '[]', 0, '2020-12-22 13:20:37', '2020-12-22 13:20:37', '2021-12-23 00:20:37'),
('d1d06967022507bfe2e305fe0e1312fc09b15613ddef6d589780b136cbcf8cde72dec3cff2ba2021', 1, 2, NULL, '[]', 0, '2020-12-12 11:18:08', '2020-12-12 11:18:08', '2021-12-12 22:18:08'),
('d231fc9875c7a221dcf9dede172abf5edf1f757531530f0fd9529a46d577b560085e2990491d5031', 1, 2, NULL, '[]', 0, '2020-12-06 01:45:18', '2020-12-06 01:45:18', '2021-12-06 12:45:18'),
('d6fa2be328f0464794060929304f3e792fa454ac97d1e618c5ceda7d8feec632e997832e27bc2bc2', 1, 2, NULL, '[]', 0, '2020-12-10 09:19:40', '2020-12-10 09:19:40', '2021-12-10 20:19:40'),
('d83c820a99acad152d00d7edf86d00bfac092ed8cc70fcfbaf7edb21ca44ee0eb5dc4a69432dc7d7', 1, 2, NULL, '[]', 0, '2020-12-16 12:45:01', '2020-12-16 12:45:01', '2021-12-16 23:45:01'),
('d9fac300517a4f6caabb35445e4fc67bf846841ac8b472fd9679e70dd889c1c5acc021bfde8115ea', 1, 2, NULL, '[]', 0, '2020-12-09 23:46:59', '2020-12-09 23:46:59', '2021-12-10 10:46:59'),
('dfa1a936c0687a111ce87de9a3afc28077dbf1e4b8e1e8cb5df6bcf0e0bb9a4b2cf59322f75e4b3c', 1, 2, NULL, '[]', 0, '2020-12-01 00:22:03', '2020-12-01 00:22:03', '2021-12-01 11:22:03'),
('e3361de3126cc14936f1bbb4086e77d8812fb085a773d39091a4500d05f5384fff000ed344abcec6', 1, 2, NULL, '[]', 0, '2020-12-04 10:55:21', '2020-12-04 10:55:21', '2021-12-04 21:55:21'),
('e3e372c79e09ba76b353c6f4c291c811cc479d43ba5fb3cac374afb41957e7317d7db8f2f30cd5bb', 1, 2, NULL, '[]', 0, '2020-12-10 08:53:08', '2020-12-10 08:53:08', '2021-12-10 19:53:08'),
('e5cbf5c36211d68edeb042052c1c2f4a636d833648155535b65a4decd90231da83b64a78868e3c79', 1, 2, NULL, '[]', 0, '2020-12-02 17:43:12', '2020-12-02 17:43:12', '2021-12-03 04:43:12'),
('e81def5d470c0b059da4eb6e4976550ad8b7a19ec0d6545331eb98bacfa32a67f80e1e539a5d171e', 1, 2, NULL, '[]', 0, '2020-12-06 01:40:48', '2020-12-06 01:40:48', '2021-12-06 12:40:48'),
('eceadfa671addfbca2d8afcd7d88eeee7a928390908cbffd1c396fe867ccb93103a45ed9c28f6293', 1, 2, NULL, '[]', 0, '2020-12-10 08:30:11', '2020-12-10 08:30:11', '2021-12-10 19:30:11'),
('ee927fa6a961d875f409e30a2e784125b4cef26569b4c99c1f723b7d593b04c21e89641f0d2a78e0', 1, 2, NULL, '[]', 0, '2020-12-07 20:20:04', '2020-12-07 20:20:04', '2021-12-08 07:20:04'),
('f09b9d227e3f869e945395d31ef97ae7ade8ef573bb9743528362997f44e7d0f8eb2ca3d4aeb82e7', 1, 2, NULL, '[]', 0, '2020-12-01 10:10:22', '2020-12-01 10:10:22', '2021-12-01 21:10:22'),
('f24a56a007197c5f5af47f2ea561e13a0f5fa86c597aa4d5a2646a5a65786066efaa0249e2b6ff53', 1, 2, NULL, '[]', 0, '2020-12-10 08:46:31', '2020-12-10 08:46:31', '2021-12-10 19:46:31'),
('f61ea3c387ae435d11a1598277bb76e72dc8d32bec865d30265294b131393a940f1eaf2dfee8fffe', 1, 2, NULL, '[]', 0, '2020-11-30 18:44:28', '2020-11-30 18:44:28', '2021-12-01 05:44:28'),
('f79c919b2549b12f18adbe928d241676e5034626c8638f96fbde6cb2929908ab5c12660c15ae5a67', 1, 2, NULL, '[]', 0, '2020-12-07 00:28:56', '2020-12-07 00:28:56', '2021-12-07 11:28:56'),
('f8e002a0942ef5a3a3475e92b5b92b3f046196e41c7e3c49a90c4af4032940c6540416f59e8d19b2', 1, 2, NULL, '[]', 0, '2020-12-07 00:06:53', '2020-12-07 00:06:53', '2021-12-07 11:06:53'),
('fb1d81188a3e2e83f228dafe4f32c91a48aba413794dd79675ab43b3b742dad14401ea2bb50b1653', 1, 2, NULL, '[]', 0, '2020-12-05 01:00:11', '2020-12-05 01:00:11', '2021-12-05 12:00:11'),
('fcc934242c7228f1fbd6a864934af8a8f8ea881a0eb4b66222a82b2d05b245a31a697fd3a047402c', 1, 2, NULL, '[]', 0, '2020-12-10 09:15:45', '2020-12-10 09:15:45', '2021-12-10 20:15:45'),
('fdeab26c9fadfa5c0e5226554ddebec20dcc3e8a242984907a4a19e35787b3f807ec0bae44779a27', 1, 2, NULL, '[]', 0, '2020-12-01 08:56:39', '2020-12-01 08:56:39', '2021-12-01 19:56:39'),
('ff652e12c894bf3a3932e3de5e728d5e7364c928bbccfec0475c3d242a70380a515cef47e7011233', 1, 2, NULL, '[]', 0, '2020-12-01 08:59:16', '2020-12-01 08:59:16', '2021-12-01 19:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(199) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'QwclARagYsibh8jRbh21bST8ICJXlMu9p9FfuBjh', NULL, 'http://localhost', 1, 0, 0, '2020-11-30 15:41:21', '2020-11-30 15:41:21'),
(2, NULL, 'Laravel Password Grant Client', 'cJZAFK7XU0PtERWy97Pc1TmajKixNvkrSGUbideA', 'users', 'http://localhost', 0, 1, 0, '2020-11-30 15:41:21', '2020-11-30 15:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-30 15:41:21', '2020-11-30 15:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('069ff7a28296a3724cae35ad953a9ca318f4204ba0e3dd94fdc60a1e61851e12e6f19c27fdb84cf7', '9fde9d6d46a7561ff337f772dd636cfa5a2eee081daef3f17d8fc5a9294a4ed342481942a4d1b45d', 0, '2021-12-16 23:48:42'),
('06d94e72db3a890aa360e74cfecfcba105159e60cbe499c2f877a6987e7400a32db34bd6319d7314', '4624df60e4b5386bde553bdbe77a0951c9009ebabe3d8c0d48fd48f50c859e5e4729e3b1a5d90e34', 0, '2021-12-08 20:02:57'),
('08cbee0c123491da79f5746da0f6e408dcf5de341aa7ce1cc9d4967976379022040b93481df248a8', '9bc0096ea1459edde426f039f9fbd6efb18954a90b0352c4c1051913b1bdbde5bab4552b7522a31c', 0, '2021-12-07 11:04:15'),
('08ec127c55b783c4b7ac02d0b28bb87f9b37af2cabd41d21e2ff0ee8d59e08cd4d3d66135b7ea249', 'eceadfa671addfbca2d8afcd7d88eeee7a928390908cbffd1c396fe867ccb93103a45ed9c28f6293', 0, '2021-12-10 19:30:11'),
('0986add72446ecad581b577912dbbae350016b4681b17a8f229d1e89fe5f1220d0ac2a54cf987203', '078e96323cbaf5c5a181fdcdc05a1ca6134147800c35abbc713c1750c79364b31ebc6aeec34741a1', 0, '2021-12-07 11:26:49'),
('09882697740660404c6acd39c7448c987a9829a81ab6e83180f1ef315c0b4194d9e101d7e975ace4', '04998fc421fae4fe1539928660f2ee8a98f61f6aace4427fd3d1ee5d94c6765778605437543e4e70', 0, '2021-12-10 19:56:17'),
('09a73720e67c37ebc49ed9bdb1c296af8917e02dd39019e428e1fbeb20f3ad6f1ac75478443f9f5e', '446460bf50f4aa5f5c970d7866fead2a082bad79510ac6cc2b98b06a823183c186dbdbbe58e2aba1', 0, '2021-12-03 03:53:13'),
('0a52526fc80fbb1c31fe759c197a54eb32ff6482a3943196e7f35cc577ceb72aadb8815f67baa045', 'a14de0dc95b1e78125b6701214417d4bf223b5e1724ac81c60dc9ad66df3714fbcebb2755d7dae05', 0, '2021-12-04 21:39:09'),
('0bccc1a8f3b0c5975e2c914c716c7b161c9fb58e8ddea0bb50ec3f5d3d507b2d8b7f9280697c2480', '8d1d07b40d5e511a4d7c5f2f6c63f18fcc1cbcb29e079665606aa8edd3ba706075c7af72da0bb55b', 0, '2021-12-15 11:37:00'),
('0d4a527051b51e1bf95920726d919cee6049f3b94ac05c321438b6160486d84d889e356f61cc5f35', 'b1fe7f207825b3c584b9eb66293d5d130a99102f81eff0ec127c38c7261ee5d6215c8ed164fca362', 0, '2021-12-03 04:55:58'),
('1039549a25871fd1bae8ab5badc7196e96ac46465ce2bf6de6a272fb37ea1168a76f1f39f0502f13', 'c1b78f04d9f7fd1f9d77afe322db515508a0b76f122f3ddf7b02c0d4e36cb3180990c168fd1399f8', 0, '2021-12-10 19:33:57'),
('13c4bd45be8543162ccfea854b6925651fc472945cc104b8ef22e8fb56cea1c6b2e73fab9dcd909f', 'e3e372c79e09ba76b353c6f4c291c811cc479d43ba5fb3cac374afb41957e7317d7db8f2f30cd5bb', 0, '2021-12-10 19:53:08'),
('155eeaf81a9bbfde30922e63c4b7076e0ed3735d4668d0a93665b853d83e84f318c2dae5d5ff3f1d', 'aa69f918a58c039512292d6cfdd69f45ab14c43918021abb4a6903d858aab971fb1c443af58414ba', 0, '2021-12-03 04:37:28'),
('167a4ce552a0ce02683b00f15345244dec793af30869b874a693dd0c0d1ce6d0f67f04a34f4dc46a', 'a9db58f63ed6f1e3977d4ce526908529ea6aa4f097c2b422a7ab7e9f52ca62478cd32dc15b6c0579', 0, '2021-12-06 13:04:34'),
('1b201bb2fedcf51fc65f58d957bce7825a3a1fdf4c33c7194bff2a86d9024488ea8aa16e52638120', '762304e5139e17fc5f558b175e437823df8e5c55ebd988f3a64285dc7d7dfef926f41da92e954fc5', 0, '2021-12-01 21:10:58'),
('280c809bff51fba79295f3421d8733ad29360737cf61c9c4c49e98c7ff21020ed55e8fc98eb6f002', '3ec128be1c6e19279c8fb69d2c68a663783cb59c9931b40caaf33cc9de58bd3d3fd15c09cb8cd052', 0, '2021-12-01 05:54:17'),
('2a27e6638c18ad896ae4296000d26a6d10ffbedf735b1a89a01f4cf6548ea05085ff5658b0c799b9', '56e7892a51dc43c458ba9eb003448c23e7dedf1272e12f02c67cc1c2b96790533f6cbdb4ab7b3515', 0, '2021-12-01 21:09:05'),
('2c0a9e8ce006f637a6fac8259e22f3861c1062af18e1e569bf1673962b2d5b138724c63e95f394bc', '93dd53db1f323a20a043c07a8c9e63d3e5a9cba53742f9f9cdcdfb48304dca7075d45061d8700d06', 0, '2021-12-14 11:10:53'),
('2ec0a474eaa4192701e6f60f5d020b02dd102722f0588762a75751bf8ab00dca669b91886ef05068', '485b3ec909c13ad8fa67fc08fd028b7605295cf625c0b4f0a1f1a8b3d6f5a98e7082e7c492cedc68', 0, '2021-12-13 10:56:40'),
('2ffbf50521935a44b6bce1b661ca86de8c7d359294ce04baba092ff3e960ce84f7c537a1ec930afa', 'fcc934242c7228f1fbd6a864934af8a8f8ea881a0eb4b66222a82b2d05b245a31a697fd3a047402c', 0, '2021-12-10 20:15:45'),
('31123ed05aab31822f2be6245dfb0740b0c0ffef7f098bf8ed4d555fe09d253483b865ae5f33ee48', '471aee99b15bf82810d16c8f8d3b14e0fe1b43e6b36f612550032df77c3cbce6e5787075ff904c8c', 0, '2021-12-16 23:39:28'),
('3118d6c894e0e22ed6346733bae0ce7ef274440ea02ce49e6dea6c8d7a25be3935a97f78214edb86', '903b99f7b14f527525e4522b6f932175700162d6102ab07d2119629a7d546a54a928cec0598fb901', 0, '2021-12-10 11:16:39'),
('3236ad3fd63b4e19899820cdffe00e0d8ca6559bb0f7a95314f4568989aa5b42c0f65b2376fa1141', 'bcbf8a8d7e602c45a365963dad58b837d6a23c5790ee44313e92b9b9537f613853853ea0e005a4fb', 0, '2021-12-07 11:13:49'),
('3992b6b758416c2a6f13d7d7527c17cd22ab96ed743e188d3b62fe3f267805ca0f8364997089c0e1', 'e81def5d470c0b059da4eb6e4976550ad8b7a19ec0d6545331eb98bacfa32a67f80e1e539a5d171e', 0, '2021-12-06 12:40:48'),
('3a202166059302b769ce5e2f988754d130b1d1f79c32cc4ee546283de1a1e0346057d7f35d81ca9d', '42e36fddfa28d55e8215b3b59adba6b02d413f9fdb83ee72c83b01adb217ad323928ac1742ab6067', 0, '2021-12-06 13:03:34'),
('3bb895b628a28b3665e8af0a4aa85d6928cd898ff5dda4298f97993fa80183aa5c840e5426a79d2b', 'd231fc9875c7a221dcf9dede172abf5edf1f757531530f0fd9529a46d577b560085e2990491d5031', 0, '2021-12-06 12:45:18'),
('3d25f6b2d4573a26ce9e017f9fe1e212ecc8cc4435cbf487cea6fac2332a7827e6092a9d3bbd7b4f', '06b216af436eb615a136d27379766d0dd544e97fa1e85621632cfd70b16546ee373677ecec864457', 0, '2021-12-01 05:44:19'),
('3f5888b8da07fa947dbc1e825f75d6fda80aa58772348b6b3c8343bcc1b71c677ca666a228560e2b', 'ad13ddea1b6d68d8d6c756f08f6dac004a603bc3c67029ac131e19f6f87a6785bfd18c7a13c5140e', 0, '2021-12-01 06:01:09'),
('40fcd6176bba78bae6386a81939ed96de2a0fea1cb6640a5019d5af7c81f5c9c9bf0f6da8bec8308', '3e928fd4fdb588f8269aa8da397abb4c1534fdef23384e642493f0dffb36094d4708855d9b9103ad', 0, '2021-12-03 04:44:31'),
('4529daf6b20f217883d94e1f04bc0c994dfff7e2eaef69e436b3f152a0b35b7e119187aaa567fc15', '9f93f2c0475a0d4019af836c3eca14024850b1fdb4e4c6e230264c67397a898e985df251cff02335', 0, '2021-12-04 12:30:04'),
('48458ec5704019aa1ceaccde29c754fd707f3d9b9ef8ee450312fe03f7739b21b23d37c9c1180c23', '4e6985626e39fa8d8133893c72963e7cae3b7f4fcf66bab1831c46cd70313abb86a2e8ae9e495bcc', 0, '2021-12-06 13:01:45'),
('4915bdd436df51328ae150d929a267121f9667312286df7efffc1a0d4d0997ecfb781b3f58cd793a', 'cb0c9a292114059cc019e27eac82b3a5c92876903583424b44cb86da4d4e9b64d7d8ddb522dfea8d', 0, '2021-12-07 11:00:51'),
('4be522c58df2bf622df616154f9c28a123c3983d6e06bfafd9d617c243867f600fb17d1f8a3e55e1', '34aceffccdcf6af08d36896316bd4138346875dd38c1a96c6ac50dd1a4288d2c1226f2327993c78f', 0, '2021-12-04 12:46:24'),
('4bfa741df0bad0420f8b7e65a122aea1f7082c13d78b088ccb0306f4581b341fc8603e715d8e0c43', 'f61ea3c387ae435d11a1598277bb76e72dc8d32bec865d30265294b131393a940f1eaf2dfee8fffe', 0, '2021-12-01 05:44:28'),
('4faea3541f3b8f8484f39e2f56ecd7d57beeda0b9ed7565b3dee586970c3deb3ba8d62d43f69a502', '59331916bf4074783abca90f2df21856dcaf7ab2d6cfa2d76de7855095fede3e42d3fec23a65cd74', 0, '2021-12-10 19:31:55'),
('52362b875be403639b41429196fecb88cb5c9e104ada133a3a2525538cf3e3409e0cbf68ca38eb26', '323614994de9202c0e51b1d8f4dd84d23f65de33d1bc0c9579b823f11e6e19d8b1d9329a7608964c', 0, '2021-12-03 05:01:55'),
('52829a4a6ad1391a5ed2350e16fdabe4f532f39abce105120c205f25bb95077a56a783203145db4b', '4870a3d9327db68ff9280ae4fd9fae86ec74741a37adb0edf78db48af229b42f8c2c5bfc487e4735', 0, '2021-12-01 04:26:50'),
('543cfddf204df3050a84fb22d927198aaaa5a4ca0bd431b014f784317e2ef00ca2cfa2d96ff2c153', 'ac3fe22d661b94a92fde394cee15bba290e50bc23799ceeda864c9992e7d5924f3106ac82f5db272', 0, '2021-12-04 21:53:36'),
('55cfd0c9569cbbde316516df343a91e22e09e0a8f1b3460b92f29db0e2facf446dbf18ade935251e', 'ab1fdaeddaf6434f6f088e472d306a527575953eecb2e86fd2ac8b2210e40dab0cb153dd93a972e3', 0, '2021-12-06 12:50:30'),
('57124cbb2c4b70ef8e79351333639e87394fb9998f685440340f6df60c6f07687c0fd215aaef99b1', 'e5cbf5c36211d68edeb042052c1c2f4a636d833648155535b65a4decd90231da83b64a78868e3c79', 0, '2021-12-03 04:43:12'),
('5b56acfad78635dd3cb64f4425d6b0925b7c93dbbcf75d8a02f5708cc54825c5334bce86383d3358', '6922681680f35d97e3b8c05b0f6d1748c7de97c0c26186d53c568bdbceda8786efbee3ceb72a58f3', 0, '2021-12-10 11:29:57'),
('5c6e008219535a5a043f76703e8d9d0fdc00aaab94f22433c2f8b3a1b534647a4291c7905825fcbc', '0063442d4d4f460b72697f1fe92e26875e039ac4c8746cbfc785cff7cb6831d283561106e07928df', 0, '2021-12-07 11:27:26'),
('5c9df37686daaed6e2d896438228a064454b461fe9c72fd68e0b318ef8ab35f5ae65081691316406', 'ff652e12c894bf3a3932e3de5e728d5e7364c928bbccfec0475c3d242a70380a515cef47e7011233', 0, '2021-12-01 19:59:16'),
('5cdf539553237e1e105368be1b1e2aeeda93ffd34c17849639a8eef87407c3bc71ce3c71597052de', '6dc3c42947b60c06681302ac561819bf6f6840d9bb1c84abd49440d0f32238b7587d7cc905b8dfa4', 0, '2021-12-06 13:07:50'),
('5cf83f17e4ffcf4df564ee65f98815e84668fc4721818a56c815f01ed609c83e7e80f3b27d8f727f', '8ecd31976079a4685dce46dee9f634700beebff5fcba8da491f5e6a9ac767d85d0c3e29c1f2d1441', 0, '2021-12-03 04:57:18'),
('5e2bb894452c891d8c5d176fa2b8f717804ef84e338ba918cb474530557e24f995bfb5ee2ad1478b', '2bf9c28ed269cff0ce3fadc608144add2a6d210049405340a8ce73ef31bb3826b16ce5b8b9137531', 0, '2021-12-03 05:02:09'),
('5f13fc5b89f4585a2f60129f20c36185b502e726b07452b8cd4f81348eb1ed46f33e2c2c38b02a30', 'e3361de3126cc14936f1bbb4086e77d8812fb085a773d39091a4500d05f5384fff000ed344abcec6', 0, '2021-12-04 21:55:21'),
('65994e08b25b537a5e8d80d038d540f151fe36efbd604fc29976015648ebf3a06eb7f6e9625c96d8', 'fdeab26c9fadfa5c0e5226554ddebec20dcc3e8a242984907a4a19e35787b3f807ec0bae44779a27', 0, '2021-12-01 19:56:40'),
('69115ff4a75529c20ff3b799b1123e61f821ac2b52cfe7c45338c3d1ea3e882bca7ebaeb311b588e', '8b99fbbc4ddc7f43131353b8d7fe1bab934fa5687324af6a7efebab27debade2db5fb27c990e3a00', 0, '2021-12-01 21:06:16'),
('695bcec2545b02152d599d91860838c8d720970f110c816700495c187b13ef67684592340d2fa175', '95492b34fb05d27422da944b27293743f1ac5ed6c8a41ceaa54786afcd25ebf110b58b58970d10c4', 0, '2021-12-05 14:13:27'),
('6f417fbd77e94a25ae212348a4caa2aa801e9c314421e365d0813c69475a1759944a5da6711ea6ee', '8528a5a57f3926aab58812881f6be2069d2a193477503a30109e38ea3e45a73e82a297aba952a33b', 0, '2021-12-03 05:01:25'),
('70229c1efc2583554150fdb3ae080a687988052a033cc78b54015035b7a281ac04b3f1da972500f4', '405736d8cb002691d3d9066229cc3c15ad91b81d9eaccab60f5e2988f9a2aab635182ac272d4996f', 0, '2021-12-04 21:53:37'),
('70cd5a59ef9c05356f48c9ce7fb5287847c27b8e73f640760ecb81448236f600c52727b177c36210', '579cffd4427f53f8df51beb60f2cb0fc16ddf72e9f67397a19a06bc3f26a0c8784750a5e0135c89c', 0, '2021-12-01 19:58:21'),
('72432ca165b42760c9cd7498c88b07e16e118b81f4f087caede52dfab370e2ece2c6a3f6a93c6cce', '365e0f6aa512e6d1950110b27fc33d56429d22a2d6abd2a8a01f4a4165608f39f74430ba192792ab', 0, '2021-12-10 20:32:58'),
('77000ac8ed5f6cf7675f092b1cdbfbbd7845bc0924e939c2f279f1cfe1c3fbcfd79ee75831b5fd30', '853b16d767bfabf6c518e028a9fbdf13ac80014b7d9e9150328cbd2fd68fb7b74fcc9fd20f71c03d', 0, '2021-12-10 19:47:42'),
('791517f3770c7ec472a7649d9e7c6f93f507acf246e711b90ff999850a5a5026bf65efde4b61d6c9', 'd83c820a99acad152d00d7edf86d00bfac092ed8cc70fcfbaf7edb21ca44ee0eb5dc4a69432dc7d7', 0, '2021-12-16 23:45:01'),
('7f59160be85298cc4c910995e264d046bc04b20d69b0c513515b7d175bdd43cc2462db1b3cbe7f65', '3cfcce2604ef1c326e92b9987dbbac73360b7d7b04f1e2facdd84e3769d88041b73e1ebe75718ca4', 0, '2021-12-06 09:19:32'),
('80459523a538a63b7c351046e5b4c9d08f8fc5e0fdcbc3d964e5d0499b00f626dc786673600c89ef', '3709f2bf64583376e62a04533ceeeaa9e2ee7ff7f2a208fc4f0a0c83591a51512ecee9de50f1b032', 0, '2021-12-07 11:19:10'),
('815334840b4d1e8a240af9523fbfb0722b84f5cb9a91835a2d9c4daf5e931d3c0b586f80e5dd4474', '3a7b0f7e4a6800dccc43da978f181ad840fe18e22cd03164a55f98480bc496e32324d3ca4b96ef1f', 0, '2021-12-10 20:18:06'),
('8206862d440aa689034386e6c66cd695024ad75045009a5d0e2f2ca37f7e9e87e20dc8fbb2101d5c', '9cd2ee72f53fea095c3a944abb925bae101ba6a1ba8023d94e99044d9d026de4fee53fffa2c5020d', 0, '2021-12-10 02:04:41'),
('86a315c90914624be1f6b4594ccced2b2c6653cba2939db42bca4235909f011d65e7e3b5a30b52a9', 'f24a56a007197c5f5af47f2ea561e13a0f5fa86c597aa4d5a2646a5a65786066efaa0249e2b6ff53', 0, '2021-12-10 19:46:31'),
('871adc8340bafb79b4cff6d893c1ddaa37e84b96afec2ec44d0c1f166bd7275882d868b1c9427aa9', '0287750e05cd76ab03c3b0d4443b73bdf45bbbc1a1e628c39309cc76d4b57ff6c41743f62d359d47', 0, '2021-12-03 03:58:35'),
('87ec1089d8e4223d1ff1cc8243573e8c4ab37c3a04e9ccff75b5cf5723810675691020648fd3081a', '5ba5623e0e5ddf1fec1ce70480daee6ad9f3ce957c777880e674e4987364bc251e93bd05dcc07c67', 0, '2021-12-01 11:20:59'),
('8c359f1e087889575fa4ec967f90578674cf223366a4a80d285a40d70869dbe42838a31951765c56', 'd1d06967022507bfe2e305fe0e1312fc09b15613ddef6d589780b136cbcf8cde72dec3cff2ba2021', 0, '2021-12-12 22:18:08'),
('8c646b2317834d9c415aabdf19f03742b56aa3ce041ca861748e7589ad7aaf207eaf27736437c3fb', '5d76578f0b16230eba760470a15bf5ddb4bcc3285cc72b1448b4bad5109d6124d6bcd212b5afff54', 0, '2021-12-03 04:58:20'),
('8c6e7088478356e6555db81b79c04aa8b06025c76e7270979e08c05cfe15e245a4d8d68bb5661940', '5e5189c0336a1773d857ac33959ab3687fdf94c4c1cca76e871b2f721ca234b716019c4cdfdc0f8c', 0, '2021-12-03 05:00:22'),
('8e61d95373d85badade3f39b85db759f155f7b52b91feefabac03aaecc0800364822bafbba93f419', '77f3d473a53cf138d28af0a1a5134fe7c880bd4f9b650196a3f6d112dc559666b454192cc8c7211b', 0, '2021-12-06 12:41:26'),
('92765b50c0502a76a43b261cd72ac871c647e7148fb4492edfe1ba9576482de3367966d9a84f62f7', '52fa52aa4d7f8193abf40297b8eff8a2f357a9b049e83d46774620836eac1be82f120813b64b0977', 0, '2021-12-06 08:51:05'),
('96a3f5eb8e53acca9e606c07aa79ec8e0509ca81404fb2b33042ca02f34ff5f3fb1cf73e9225cf9e', 'be52cb0371321644152944ced2310f904f29cf6e238e7dc0d3af88394a847f20d8c850e83d3b9287', 0, '2021-12-06 12:35:17'),
('986a6de507c6eade52cd33d5dc1e9c747c1739c6f6dc7863f06d5ba13981d3039ba549ec1e9f3655', 'dfa1a936c0687a111ce87de9a3afc28077dbf1e4b8e1e8cb5df6bcf0e0bb9a4b2cf59322f75e4b3c', 0, '2021-12-01 11:22:03'),
('9887e43dc818a14096afa4bc0f5738833f7c5ee48a114a4a72dfcb62fdeacc7248d6df6056b74391', 'f09b9d227e3f869e945395d31ef97ae7ade8ef573bb9743528362997f44e7d0f8eb2ca3d4aeb82e7', 0, '2021-12-01 21:10:22'),
('98fd6874f53028d7679c97fb4bbb5ab74ddb47f49b53a2d53eb7d2659a56ddd7c5bc126c6b0b9e50', '1e0a6cb417b4f502d1729c27b456cf4e82091f12cba2690d92a214b3fcbe341ebb3924e9b211636c', 0, '2021-12-03 04:00:59'),
('996352768e6da2c754e98ff57156cce3d3e003e66cea9876912a9527a7d62c3481a3a898a6539126', 'b2f68bb4f7effff6c879c037714fc8b5c1aaa0ad17365e1bd5c883fbc399cca7d1d5160e431c4739', 0, '2021-12-06 12:21:43'),
('9b23b23de19ef4c46fbfc6220e563c2350a08c2c165cb3f686930140a84c90f8f4d819c29d8aca4e', 'ee927fa6a961d875f409e30a2e784125b4cef26569b4c99c1f723b7d593b04c21e89641f0d2a78e0', 0, '2021-12-08 07:20:04'),
('9cc4d9acb904830d32815a1418a6edda2560f461f98a34e359f3d6d52d5587e85e6855f0f9e13149', '6d972c9bb738ab340cc67ea59204f39e6db0ec61bb3bb76c05315ab01228733fa2119328eff271a2', 0, '2021-12-17 14:27:49'),
('9e12a7518c1ff5e34100093f286bb140a1c7f2675d964dd00963e4bf350395637b180bd15bc2fa09', '19ac6cad659c366a5c1aba4f67f8f3f4df4aa4874922518ca2e389c9b0eb2b3ac8443cf3c9992ab7', 0, '2021-12-06 09:18:46'),
('9e5f51271376f0679aad8c6c1d6a81b571552f75cb5e9bcfac2cb3a75b1dfca990b84a67ffde5966', 'c19218759411d9da8a2eb39e5d439988943e71870ff7ff0d9856f481911b0c28784b8776aaa13ef1', 0, '2021-12-01 21:13:06'),
('a10248e23448b0b4a4a9261908ee2425342bdbddf0ebd8d476f2d9bd7d9e8fb10b8dd09f87aaaef9', '2a2cb554610342ee37849d29acbe70894391c2934d55cdc8276c35ba8585b561e524f5d5d4f3e138', 0, '2021-12-10 03:34:45'),
('a1f691cbdb851f852f7acc5eca82a7412f98e8bc6cc38df4f8bf4bd43b22551f0328cd5a908b8563', 'fb1d81188a3e2e83f228dafe4f32c91a48aba413794dd79675ab43b3b742dad14401ea2bb50b1653', 0, '2021-12-05 12:00:11'),
('a1f725edf05b142b51205a5802635b9505beb1f78c5b9399fd29042b7ade0a65fdd5acd3d15416ea', 'a17b240b5964f9ba1c7b6341301cae1897df4ea7cca1d3466208ede27dffb9cabd587fe3a8debf2c', 0, '2021-12-04 21:45:08'),
('a32c87296c43af71bb355cd2e57e159d4357a0971584b0048ceb16e7a5e4147170a8a6130f05fe2b', '9a1d0fe9b9d6586589f0706fadcffb92528a3e041519cfc7c98ad44b9c39f0db394b96e2452a664e', 0, '2021-12-16 23:39:26'),
('b32b7e2ab8c765173cb2143a476e631f8948d98b59e36c2e8dce4bfa96d2f8d61d315cbe4042335a', 'bd6efb8f07b1d23f57c458bfbd930d0bbc306b868e5e332b97ba2ede1dc64a0fba5b117466af72b8', 0, '2021-12-01 19:54:38'),
('b37cbdcd9c858c568dc315d0c709f5f082d2b37d851278a956140cfd2d1198526039bb72852a2529', '0f19bf0ce71fd3447ea0c5553fc65b3f6b73547d762080800ec62fb9efc0306187af0517bd300f6a', 0, '2021-12-04 12:31:31'),
('b55f23cdd1a2ac6a1b367c693a02577a527997c1e84368a54efad5b4ac6b30baa009de934a97c51e', '6a1187177a4b720a9d5bb56afe5cfef20473061bd080021921306cc0646a774b4bd7d07165c0539c', 0, '2021-12-03 10:20:27'),
('b85fb9255ac4663dc34758f88039e017e0f46832b927714e69b413fe8b7b0406c794779cb1808bf5', '844f14067a8b8d742adc70fbde921638f8d1649d75677a362c10bbea987f6ac1c1012c5e10295d10', 0, '2021-12-03 04:59:45'),
('b8a0fd2b6118fd5e5cf37551203af659f460f9f6a77cd282f26578bc5b01c9334b2d97477615078f', '9e2d5eee983856ea26abce8c21c43945f781e62a80885444ae90b9b162162b13c5dbb9d84562e82e', 0, '2021-12-10 19:50:03'),
('b9eb5c43d692eb8a35b54e1a145f25db87a585483083c6e4e7d4f197420eac554d85817174dc023b', 'cb6d56194ce7c999c508adf4e6d8b9fb114c0db0aee36b2049fb6c904c24f36b1f64e8f32beff1c2', 0, '2021-12-06 12:43:30'),
('ba7fb1dabc38ba0a637874c1045cc352db3713041925e56da1bd0543ebb5eccca3694f6637ad6c9f', '0658e166582621cff07299f0bd2963a009861d8fdeb4ec81919171829e82ce665cc1f3b9b14f5bd2', 0, '2021-12-03 04:59:53'),
('bc1628e8b1e19c2f1f02d0998ec7baf6797ba6151abe5c1becca907426b7a7bd69e4f62420783839', '648a7515c705ecb2517ea347d728b7b80a6bcd5bd801846efb653090a3b04116a63cbe337a336b9e', 0, '2021-12-01 19:54:25'),
('bce1fcdcf5289c09a8747d6506b6fd3df78ce61b8d19e238c7b8ed75d4b08e8262623575f56a89ef', '5405a158e333c396e390292c25be1a935a6f18a4942eb300ca08e85865e0174a32dc411d525aa54a', 0, '2021-12-10 19:51:27'),
('c117d203d2eb309baf6988d847c9ac8947e66b014d4659a8c4c94439f8c480b54de2625af1589e4b', 'd9fac300517a4f6caabb35445e4fc67bf846841ac8b472fd9679e70dd889c1c5acc021bfde8115ea', 0, '2021-12-10 10:46:59'),
('c26a079aaa9d5aeab161d3776c63d9940ddd6bd143cde16bb9cb10736c09a2158789d87ebc04eaa9', 'f8e002a0942ef5a3a3475e92b5b92b3f046196e41c7e3c49a90c4af4032940c6540416f59e8d19b2', 0, '2021-12-07 11:06:53'),
('c28effda341d1b1629fb3a204633df6f66cd813b05c7e8d3b78b7646fffa9def93998e5fc4fd94e0', '34ecc9dab8b9e5dbe5966b4c69af9c575afd35955a7968bc0c7dab9befaf8d8de4351f1eb8803a6f', 0, '2021-12-16 23:45:23'),
('c5a770018ceb318bbd46578140ee28d3a93b26d92b713fe6829f890a03a96a19645d71c29511334d', '308912ce1695cf5775f009e3deb83660d4769a362104e0f7513ac7301ff95f11ecb2e9f57de7ff34', 0, '2021-12-06 12:34:58'),
('c63789713efdadaf04440da21e65996fd13f6278c1ac4927de0742cce9e0115fc7eda0213b3b50a7', '6e9cfc190e569e45c8eed09c863ce982df7ac89c94396aa609b51874d787ce1c436a4fdc41709dc2', 0, '2021-12-03 05:00:43'),
('c92c59df98845fe914207eec576d9f79028a8efb04f14b9c5e3026f5fb118728543579cdc8c762e7', '63cd5f849b75c1c8f7fa802700517933ecc7c521fa0ff5e7c0e3a84a5451f2e9b1588f60e0c9bf79', 0, '2021-12-01 21:07:20'),
('cd479d31ca351d32af9f6753d031c48241c806f824eaa3077ac5fad6689ae30149599730f7f4366c', '0a57e9fe6e4e286ec8e973f9a10142e894150cb9343e9afc7277e88fe628fb4193ba764d16b7c933', 0, '2021-12-07 11:22:13'),
('cfa66c0f4e219d4fd35116c298e8b77b263678b5d80bd9630b50d3746cb3d7ed724a0cd6993660e1', '4f3ad69bbe32fa2f9499e9b14198374f0bdd60467e5bd8d2c0e4c5363ec79c6b85b55a02ea47b5a5', 0, '2021-12-10 20:44:35'),
('d0da70d6a72491ad6e8e92a37f3283102af33c9fadf000a204b953cb15f3a6cb1ea01a7fa3e14540', 'd6fa2be328f0464794060929304f3e792fa454ac97d1e618c5ceda7d8feec632e997832e27bc2bc2', 0, '2021-12-10 20:19:40'),
('d388983db8a746a08275de4f19be9b1fbb6631d7cb7295e4485edcd74f579ccf6a328339a2bd712a', '3349529f9d3a4d3dead3ff4ecab524eefa0797b781f5f3dc2a891f426538288f66f0e4ecdec96565', 0, '2021-12-10 20:22:15'),
('d43fe0500589eb58ce99964f2ecaa8a73da2b780e47e655f597c60c39a74c8c2d5f805b0a65ccbde', '75a70cbe337ed7007cf5d48a77cf3800e5bf047e7c080d6326be31085dce4f2cae77239183397919', 0, '2021-12-03 03:57:38'),
('d48469ad08f56de059315bb5e5b42ebd33d37ef80d938d5391e49d53a6f955d50a97a11bf898ed32', '386dc3ba1b3a2019779cc8a1acfbdc44d8367b645140dccb80bf67aab553c0dfc80b8069a60fb645', 0, '2021-12-10 20:18:50'),
('d508b5bbd574ea65aa6adc3a4bc4b881c4b34a7f938c59147401a16dd5f8960c1929e4515960459e', '0cce86bd9760f7a747db1cb3870ed411ce68a0d31eba99b292e29cec008b415e7bccbc17bbe5ca1d', 0, '2021-12-07 11:25:30'),
('d5c475e33b2cf31a765b4bcf60b5ed3f7fd1b3511f21ffb8d89a955867272e2855ece53f3afab348', 'cf304ee769dcd790d23b51f560f82c13fa57be3f21542cc382625af04417a3a0266980cb6f0e6f82', 0, '2021-12-23 00:20:37'),
('dad5902af2f21ef42a345c0b567f8b09b06fa7b0c494a5a4857933965a90c0b8bd3f302a96bca36b', '29e8e0ae17938f2da63b6ab63a102217ff19ddaa99e9e05823a008684d421cb602504ed1664b2023', 0, '2021-12-06 12:29:16'),
('e05cacb28baecc2703e15c9dec31f2598ce06b251cc68cbb3715c08a2285d289c1dc008e8c55f8c7', 'a46236845b7a4e49e2eb9d164d13aa0a969f0b62494935f4bf292ec7b8ac719dffba8da63eb65039', 0, '2021-12-04 21:43:58'),
('e12cfed7569dc4f1efc7d1b15928cf3a28f098bb8b6ffa75b9488027f1e1d224e194f705365fd881', 'b31c9dd60c731c077e63af9d6fdd071037aa0ef0e758d9b491d7d7feac5be58fc99f137f8b332769', 0, '2021-12-03 03:59:31'),
('e5f8111eb13b1754857d6e857668abe021a304699e9bce68f374478cc7f645973aedd31ad36c4fda', 'acbd95f8920cf16235323951e47c749b7bed924f2b5052ef14748041ead842a9a43436cfe34e4be5', 0, '2021-12-07 21:04:53'),
('ea93faf874cb0e9071428e2d7132bfda771dc933ca888dea249014c9fba35f93704dc13b13f33bda', '3159de67b41f6241bb710b7a9ee571ccd0f809f1ae36a2c9dcd8aa6d4ea2981d24c9952a8ee96a0f', 0, '2021-12-10 02:30:57'),
('ede0413252d85b5c1f8e98d69aa872e55b33372ca81191542da782b83e1afeb7326899c384f90f91', '2f4207615a62060bb6fa33bc2d9df4ab1e1b0781b913917047713980cf612cb25e56262460ed99e7', 0, '2021-12-05 12:02:33'),
('efadc3d8eb0d57f8236ea536548a55fad3ccbc126b6d77a33e3ba8246dfef7d83c666965c0143f94', '27cc1426005c3d65edeabfc07490e105574759f95fb6902777b55c29c1e729bc66a12066a669fbdd', 0, '2021-12-07 11:32:35'),
('f21a2156dea7d6c8a9f9962f39b00a0828d59daab591d4482a24c90609e03af3f0e54ab4db63fb8b', 'ad70cbca5db910fba25fc695163ab6c7a6d9f8bb05b2d355273c2ff571258afbd690354bb6b76914', 0, '2021-12-10 19:27:14'),
('f572bfe4825b942b9675611b9c12aff043a5cc69132c7ab084ac5c2373973c5af60bb96fa767ace2', '27a66d05751f9ef4161b454172f644ce91e8ab79b5c6f6fee5bf786a360e3c43db22599a6f5bf2f6', 0, '2021-12-03 04:52:57'),
('f6361100944153d639b3b6da4feaac4f41c5c7d90b3a5309207eb1532325c7df990e806a5f691032', '2e9e791838700071bc2c08394d3b6d1d0ad459c9818573a24bdad0410e4343aec97b935e6d4a6de1', 0, '2021-12-07 11:21:09'),
('fc788f3ddd944f6932235e169e194c2eace460af5e778be01aa7c63edb30959c079885536ed82f77', '7c2f4661eb67ef9cc8840e11a9c121f76bb303bdfdbfa4ef80737bce43181d05f1ca689e3c8044b5', 0, '2021-12-10 19:32:48'),
('fd91ab78df155f7f11e834a933829b067a447436dbc4d8a1aaf4bbaba50cf3530a7d58d09a7380fd', 'f79c919b2549b12f18adbe928d241676e5034626c8638f96fbde6cb2929908ab5c12660c15ae5a67', 0, '2021-12-07 11:28:56'),
('ff861aebc95434b60f1015c08430e124ca075d41dc0cfa3d6a209472fe6c49230357ad0cedc35609', '7ad5aee633e68d1d28f18b73b808cec7ad4c5fa1d2cc5e2593c948c09fa4a3b0ae8228a95c858821', 0, '2021-12-08 09:27:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `alternate_text` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `image_size` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `case_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `name`, `alternate_text`, `image_size`, `case_id`, `created_at`, `updated_at`) VALUES
(1, 'M13310_L.jpg', 'Mixed Red', '', 1, '2020-12-08 19:54:25', '2020-12-08 19:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(199) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Ross Parker', 'liliana20@example.com', '$2y$10$fDCozEwg6PSwFkrhRh1De.V8qq0cRM6G8Z6KJerUyXC4H.8ztfOsG', '2020-11-30 16:38:07', '2020-11-30 16:38:07'),
(2, 'Lionel Parker', 'elizabeth94@example.com', '$2y$10$0FZyc4y71s61Wjl5oM9k8.rGVrUMDi9B8CtYxmku2Wl0NTtNgb2Ea', '2020-11-30 16:38:07', '2020-11-30 16:38:07'),
(3, 'Amya Koepp', 'nyah37@example.com', '$2y$10$Zo87AvDtdPunrYBkCqQOsuQYAPIbDVLNcSl7JlIpdIjxiuYSTkF0q', '2020-11-30 16:38:07', '2020-11-30 16:38:07'),
(4, 'Mireya Heidenreich II', 'fparisian@example.org', '$2y$10$TRN7Py6/bkGbSnu6.g52Y.AiDxNeuHh4nXw./W5io8F4kxJmBG/ra', '2020-11-30 16:38:07', '2020-11-30 16:38:07'),
(5, 'Miss Adelia Lockman', 'thiel.harmony@example.net', '$2y$10$nSUP0sQZ13.WFm0Nz9Bd.ea2hGCRzPi500hfTs7R9es.N6eFAQUnC', '2020-11-30 16:38:07', '2020-11-30 16:38:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bottles`
--
ALTER TABLE `bottles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bottle_case`
--
ALTER TABLE `bottle_case`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bottle_case_bottle_id_foreign` (`bottle_id`),
  ADD KEY `bottle_case_case_id_foreign` (`case_id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_case`
--
ALTER TABLE `campaign_case`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_case_campaign_id_foreign` (`campaign_id`),
  ADD KEY `campaign_case_case_id_foreign` (`case_id`);

--
-- Indexes for table `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bottles`
--
ALTER TABLE `bottles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bottle_case`
--
ALTER TABLE `bottle_case`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `campaign_case`
--
ALTER TABLE `campaign_case`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cases`
--
ALTER TABLE `cases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bottle_case`
--
ALTER TABLE `bottle_case`
  ADD CONSTRAINT `bottle_case_bottle_id_foreign` FOREIGN KEY (`bottle_id`) REFERENCES `bottles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bottle_case_case_id_foreign` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `campaign_case`
--
ALTER TABLE `campaign_case`
  ADD CONSTRAINT `campaign_case_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_case_case_id_foreign` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
