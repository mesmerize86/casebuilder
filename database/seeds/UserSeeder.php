<?php

use Illuminate\Database\Seeder;
use App\Models\User_Model;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User_Model::class, 5)->create();
    }
}
