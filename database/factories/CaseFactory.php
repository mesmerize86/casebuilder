<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Case_Model;
use Faker\Generator as Faker;

$factory->define(Case_Model::class, function (Faker $faker) {
    return [
        'Name' => $faker->name(),
        'CaseCode' => $faker->numberBetween(10, 100000),
        'CaseType' => $faker->name(),
        'NumberOfBottles' => $faker->numberBetween(1, 9)
    ];
});
