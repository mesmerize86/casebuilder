<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bottle_Model;
use Faker\Generator as Faker;

$factory->define(Bottle_Model::class, function (Faker $faker) {
    return [
        'Name' => $faker->name(),
        'SkuCode' => $faker->numberBetween(10, 100)
    ];
});
