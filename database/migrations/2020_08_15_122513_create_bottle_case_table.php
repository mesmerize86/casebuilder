<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBottleCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bottle_case', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bottle_id');
            $table->unsignedBigInteger('case_id');
            $table->timestamps();

            $table->foreign('bottle_id')->references('id')->on('bottles')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('case_id')->references('id')->on('cases')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bottle_case');
    }
}
